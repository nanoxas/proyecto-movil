package com.mecolab.memeticameandroid.Utils;

import android.media.MediaPlayer;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.mecolab.memeticameandroid.R;

import java.io.IOException;

/**
 * Created by Raimundo on 11/1/2016.
 */

public final class MediaPlayerUtils {
    private MediaPlayerUtils() {
    }

    public static void initiatePlayer(final MediaPlayer player, final ImageButton playBtn, final ImageButton stopBtn) {

        //Setear listeners
        try {
            player.prepare();
        } catch (Exception e) {
            Log.e("MediaPlayer", e.toString());
        }

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer player) {
                Log.i("STOPPED", "Media Player Stopped");
                player.pause();
                player.seekTo(0);
                playBtn.setImageResource(R.drawable.ic_play_dark);
            }
        });

        playBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!player.isPlaying()) {
                    Log.i("STARTED", "Media Player Started");
                    player.start();
                    playBtn.setImageResource(R.drawable.ic_pause_dark);
                } else {
                    Log.i("PAUSED", "Media Player Paused");
                    player.pause();
                    playBtn.setImageResource(R.drawable.ic_play_dark);
                }
            }
        });
        stopBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (player.isPlaying()) {
                    Log.i("STOPPED", "Media Player Stopped, preparing...");
                    player.pause();

                    // Execute some code after 2 seconds have passed
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            playBtn.setImageResource(R.drawable.ic_play_dark);
                            player.seekTo(0);
                        }
                    }, 500);
                } else {
                    player.seekTo(0);
                }
            }
        });
    }

    public static void closeMediaPlayer(MediaPlayer player) {
        player.reset();
        player.release();
    }
}
