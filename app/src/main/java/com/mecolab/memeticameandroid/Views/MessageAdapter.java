package com.mecolab.memeticameandroid.Views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.etiennelawlor.imagegallery.library.util.Compressor;
import com.mecolab.memeticameandroid.Activities.ChannelActivity;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Utils.MediaPlayerUtils;
import com.rockerhieu.emojicon.EmojiconTextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class MessageAdapter extends ArrayAdapter<Message> {
    private static final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

    private LayoutInflater mInflater;
    private ArrayList<Message> mMessages;
    private boolean mMemeChannel = false;
    private Context context;

    private MediaPlayer mediaPlayer;

    static HashMap<Integer, Integer> sLayouts;
    static HashMap<Message.MessageType, Integer> sTypesInt;

    static {
        sLayouts = new HashMap<>();
        sTypesInt = new HashMap<>();
        sTypesInt.put(Message.MessageType.TEXT, 0);
        sLayouts.put(0, R.layout.message_text_list_item);
        sTypesInt.put(Message.MessageType.IMAGE, 1);
        sLayouts.put(1, R.layout.message_image_list_item);
        sTypesInt.put(Message.MessageType.AUDIO, 2);
        sLayouts.put(2, R.layout.message_audio_list_item);
        sTypesInt.put(Message.MessageType.VIDEO, 3);
        sLayouts.put(3, R.layout.message_video_list_item);
        sTypesInt.put(Message.MessageType.OTHER, 4);
        sLayouts.put(4, R.layout.message_other_list_item);
        sTypesInt.put(Message.MessageType.NOT_SET, 5);
        sLayouts.put(5, R.layout.message_text_list_item);
        sTypesInt.put(Message.MessageType.MMA, 6);
        sLayouts.put(6, R.layout.message_mma_list_item);
    }

    public MessageAdapter(Context context, int resource, ArrayList<Message> objects) {
        super(context, resource, objects);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMessages = objects;
        this.context = context;
    }

    public MessageAdapter(Context context, int resource, ArrayList<Message> objects, boolean memeChannel) {
        super(context, resource, objects);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMessages = objects;
        this.context = context;
        mMemeChannel = memeChannel;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Message message = mMessages.get(position);

        if (view == null) {
            view = mInflater.inflate(getLayout(message.mType), parent, false);
        }
        if (message.mType == Message.MessageType.TEXT) {
            EmojiconTextView contentView =
                    (EmojiconTextView) view.findViewById(R.id.MessageListItem_Content);

            //contentView.setText(message.mContent);
            markItDown(contentView, message.mContent);
            
        } else if (message.mType == Message.MessageType.IMAGE) {

            ImageView contentView = (ImageView) view.findViewById(R.id.MessageListItem_Content);


            Picasso.with(parent.getContext()).load(message.mContent).into(contentView);
            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(message.mContent), "image/*");
                    context.startActivity(intent);
                }
            });

        } else if (message.mType == Message.MessageType.VIDEO) {
            final VideoView vv = (VideoView) view.findViewById(R.id.MessageListItem_Content);
            vv.setVideoPath(message.mContent);
            //MediaController ctlr = new MediaController(context, false);

            //ctlr.setMediaPlayer(vv);
            //vv.setMediaController(ctlr);
            //vv.requestFocus();
            vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    MediaController mc = new MediaController(vv.getContext(), false);
                    // set correct height
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) vv.getLayoutParams();
                    //params.height = 200;
                    //params.width = 200;
                    //vv.setLayoutParams(params);

                    vv.setMediaController(mc);
                    mc.show(0);

                    FrameLayout f = (FrameLayout) mc.getParent();
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    lp.addRule(RelativeLayout.ALIGN_BOTTOM, vv.getId());

                    ((LinearLayout) f.getParent()).removeView(f);
                    ((RelativeLayout) vv.getParent()).addView(f, lp);
                    mc.setAnchorView(vv);
                }
            });

        } else if (message.mType == Message.MessageType.MMA) {

            ImageView contentView = (ImageView) view.findViewById(R.id.MessageListItem_Content);

            Compressor c = new Compressor();

            String zipFile = message.mContent.replace("file://", "");
            String save = "/storage/emulated/0/MemeticaMe/Unzipped MMA " + zipFile.substring(zipFile.lastIndexOf('/') + 1, zipFile.lastIndexOf('.'));
            File folder = new File(save);

            if (!folder.isDirectory()) {
                try {
                    c.unzip(zipFile, save);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            File[] listOfFiles = folder.listFiles();

            String image = "file://";
            String audio = "file://";
            for (int i = 0; i < listOfFiles.length; i++) {
                String mimeType = URLConnection.guessContentTypeFromName(listOfFiles[i].getName());
                if (listOfFiles[i].isFile() && mimeType.contains("image")) {
                    image += listOfFiles[i].getAbsolutePath();
                    continue;
                }
                if (listOfFiles[i].isFile() && mimeType.contains("audio")) {
                    audio += listOfFiles[i].getAbsolutePath();
                } else { //Si es 3gp, y no se detecto audio
                    audio += listOfFiles[i].getAbsolutePath();
                }
            }

            final String ipath = image;
            Picasso.with(parent.getContext()).load(image).into(contentView);
            final MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(audio);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ImageButton playButton = (ImageButton) view.findViewById(R.id.playButton);
            ImageButton stopButton = (ImageButton) view.findViewById(R.id.stopButton);

            MediaPlayerUtils.initiatePlayer(mediaPlayer, playButton, stopButton);

            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(ipath), "image/*");
                    context.startActivity(intent);
                }
            });

        } else if (message.mType == Message.MessageType.AUDIO) {
            final ImageButton play = (ImageButton) view.findViewById(R.id.playButton);
            final ImageButton stop = (ImageButton) view.findViewById(R.id.stopButton);

            String url = message.mContent; // your URL here
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(url);
            } catch (Exception e) {
                Log.e("MediaPlayer", e.toString());
            }

            MediaPlayerUtils.initiatePlayer(mediaPlayer, play, stop);

        } else if (message.mType == Message.MessageType.OTHER) {
            ImageView contentView = (ImageView) view.findViewById(R.id.MessageListItem_ClipImage);
            TextView fileName = (TextView) view.findViewById(R.id.MessageListItem_FileName);
            try {
                fileName.setText(String.valueOf(message.mMimeType.split("/")[1].toUpperCase()));
                contentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setDataAndType(Uri.parse(message.mContent), message.mMimeType);
                        try {
                            getContext().startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "Can't open this type of file", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (message.mType == Message.MessageType.NOT_SET) {
            TextView contentView = (TextView) view.findViewById(R.id.MessageListItem_Content);
            contentView.setText(message.mContent);
        }

        if(message.mType != Message.MessageType.TEXT && message.mSize >= 100){
            //Mostrar progress dialog
            //Registrar evento al progress del mensaje...
        }

        // Voting buttons for memes
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.MessageListItem_Rating);
        TextView authorView = (TextView) view.findViewById(R.id.MessageListItem_Author);

        if (mMemeChannel){
            authorView.setVisibility(View.GONE);
            float rating = (float) message.mRating;
            ratingBar.setRating(rating);
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    message.rateMeme(context, rating, new Listeners.OnRateMemeListener(){
                        @Override
                        public void onMemeRated(Message meme) {
                            for (Message msg: mMessages){
                                if (msg.mServerId == meme.mServerId){
                                    msg.setRating(meme.mRating);
                                    msg.save(context);
                                    Activity activity = (ChannelActivity)context;
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            notifyDataSetChanged();
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            });
        } else {
            if (ratingBar != null)
                ratingBar.setVisibility(View.GONE);
            authorView.setText(User.getUserOrCreate(getContext(), message.mSender).mName);
        }

        return view;
    }

    @Override
    public int getViewTypeCount() {
        return Message.MessageType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        Message.MessageType type = mMessages.get(position).mType;
        if (type == null) type = Message.MessageType.TEXT;
        return sTypesInt.get(type);
    }

    private int getLayout(Message.MessageType type) {
        if (type == null) type = Message.MessageType.TEXT;
        return sLayouts.get(sTypesInt.get(type));
    }

    private void markItDown(EmojiconTextView textView, String text) {
        //Bold it
        int occurrences = countOccurrences(text, '*');
        int i = 1;
        while (i < occurrences) {
            int posB = ordinalIndexOf(text, "*", i);
            int posF = ordinalIndexOf(text, "*", i + 1);
            text = text.substring(0, posB)
                    + "<b>" + text.substring(posB + 1, posF) + "</b>"
                    + text.substring(posF + 1, text.length());
            occurrences = -2;
        }

        //Italic it
        occurrences = countOccurrences(text, '_');
        while (i < occurrences) {
            int posB = ordinalIndexOf(text, "_", i);
            int posF = ordinalIndexOf(text, "_", i + 1);
            text = text.substring(0, posB)
                    + "<i>" + text.substring(posB + 1, posF) + "</i>"
                    + text.substring(posF + 1, text.length());
            occurrences = -2;
        }

        //Strike it
        occurrences = countOccurrences(text, '~');
        if (i > occurrences)
            textView.setText(Html.fromHtml(text));
        while (i < occurrences) {

            String aux = Html.fromHtml(text).toString();

            int posBaux = ordinalIndexOf(aux, "~", i);
            int posFaux = ordinalIndexOf(aux, "~", i + 1);
            int posB = ordinalIndexOf(text, "~", i);
            int posF = ordinalIndexOf(text, "~", i + 1);
            text = text.substring(0, posB)
                    + text.substring(posB + 1, posF)
                    + text.substring(posF + 1, text.length());
            textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
            Spannable span = (Spannable) textView.getText();
            span.setSpan(STRIKE_THROUGH_SPAN, posBaux, posFaux - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            occurrences = -2;
        }
    }

    private int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }

    private int countOccurrences(String string, char c) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

}



