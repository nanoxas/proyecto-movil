package com.mecolab.memeticameandroid.Views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mecolab.memeticameandroid.Models.Channel;
import com.mecolab.memeticameandroid.R;

import java.util.ArrayList;

/**
 * Created by Mecolab on 04-12-2016.
 */

public class ChannelAdapter extends ArrayAdapter<Channel> {
    private LayoutInflater mInflater;
    private ArrayList<Channel> mChannels;

    public ChannelAdapter(Context context, int resource, ArrayList<Channel> objects){
        super(context, resource, objects);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mChannels = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null){
            view = mInflater.inflate(R.layout.channel_list_item, parent, false);
        }

        Channel channel = mChannels.get(position);
        ImageView image = (ImageView) view.findViewById(R.id.ChannelListItem_Photo);
        image.setImageResource(R.drawable.ic_two);

        TextView channelNameView =
                (TextView) view.findViewById(R.id.ChannelListItem_Name);
        channelNameView.setText(channel.mName);

        TextView channelCategoryView =
                (TextView) view.findViewById(R.id.ChannelListItem_Category);
        channelCategoryView.setText(channel.mCategories);
        return view;
    }
}
