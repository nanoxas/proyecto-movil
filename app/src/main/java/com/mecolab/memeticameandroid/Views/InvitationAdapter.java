package com.mecolab.memeticameandroid.Views;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mecolab.memeticameandroid.Activities.ConversationActivity;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.NetworkingManager;
import com.mecolab.memeticameandroid.R;

import java.util.ArrayList;

/**
 * Created by Gabriel on 17/10/2016.
 */

public class InvitationAdapter extends ArrayAdapter<Invitation> {
    private LayoutInflater mInflater;
    private ArrayList<Invitation> mInvitations;

        public InvitationAdapter(Context context, int resource, ArrayList<Invitation> objects){
            super(context, resource, objects);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mInvitations = objects;
        }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.invitation_list_item, parent, false);
        }
        final Invitation invite = mInvitations.get(position);

        TextView text = (TextView) view.findViewById(R.id.invitation_text);

        ImageButton accept = (ImageButton) view.findViewById(R.id.accept);
        ImageButton cancel = (ImageButton) view.findViewById(R.id.cancel);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                invite.accept(getContext());
                Intent conversationIntent = new Intent(getContext(), ConversationActivity.class);
                conversationIntent.putExtra(Conversation.SERVER_ID, invite.mConversation.mServerId);
                getContext().startActivity(conversationIntent);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long result = invite.decline(getContext());
                mInvitations.remove(position);
                notifyDataSetChanged();

            }
        });


        text.setText(invite.mConversation.mTitle);

        return view;
    }
}




