package com.mecolab.memeticameandroid.Models;

import android.content.Context;

import com.mecolab.memeticameandroid.Networking.NetworkingManager;
import com.mecolab.memeticameandroid.Persistence.DatabaseManager;

import java.util.ArrayList;

/**
 * Created by Gabriel on 17/10/2016.
 */
public class Invitation {

    public static int INVITATION_PENDING = 0;
    public static int INVITATION_ACCEPTED = 1;
    public static int INVITATION_DECLINED = -1;

    public final int mId;
    public final Conversation mConversation;
    public final int mStatus;
    public final User mUser;


    public Invitation(int id, Conversation conversation, int status, User user) {
        mId = id;
        mConversation = conversation;
        mStatus = status;
        mUser = user;
    }

    public static Invitation getInvitation(Context context, User user, Conversation conversation) {
        return DatabaseManager.getInstance(context).getInvitation(user, conversation.mServerId);
    }

    public static Invitation getInvitation(Context context, String user_phone, int conversation_id) {
        return DatabaseManager.getInstance(context).getInvitation(user_phone, conversation_id);
    }

    public static Invitation getInvitation(Context context, int invitationId) {
        return DatabaseManager.getInstance(context).getInvitation(invitationId);
    }

    public long save(Context context){
        return DatabaseManager.getInstance(context).insertInvitation(this);
    }

    public void accept(Context context){
        NetworkingManager.getInstance(context).acceptInvite(this.mConversation.mServerId);
        DatabaseManager.getInstance(context).acceptInvitation(this);
    }

    public long decline(Context context){
        long result = DatabaseManager.getInstance(context).declineInvitation(this);
        return result;
    }

    public static ArrayList<Invitation> getInvitations(Context context){
        return DatabaseManager.getInstance(context).getInvitations();
    }

    public static ArrayList<Invitation> getPendingInvitations(Context context){
        return DatabaseManager.getInstance(context).getInvitations(INVITATION_PENDING);
    }

    public static class Builder {
        private int mId = -1;
        private Conversation mConversation = null;
        private User mUser = null;
        private int mStatus = INVITATION_PENDING;

        public Builder setConversation(Conversation conversation) {
            mConversation = conversation;
            return this;
        }

        public Builder setId(int id) {
            mId = id;
            return this;
        }

        public Builder setUser(User user) {
            mUser = user;
            return this;
        }

        public Builder setStatus(int status){
            mStatus = status;
            return this;
        }

        public Invitation build(){
            if (mConversation == null || mUser == null) {
                return null;
            }
            return new Invitation(mId, mConversation, mStatus, mUser);
        }

    }


}

