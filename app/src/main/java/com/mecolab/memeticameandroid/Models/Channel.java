package com.mecolab.memeticameandroid.Models;

import android.content.Context;

import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.Networking.NetworkingManager;
import com.mecolab.memeticameandroid.Persistence.DatabaseManager;

import java.util.ArrayList;

/**
 * Created by Mecolab on 03-12-2016.
 */

public class Channel {
    public static final String SERVER_ID = "channel_server_id";

    public final int mId;
    public final int mServerId;
    public final String mName;
    public final User mAdmin;
    public final String mCategories;

    public Channel(int id, int serverId, String name, User admin,
                   String tags)
    {
        mId = id;
        mServerId = serverId;
        mName = name;
        mAdmin = admin;
        mCategories = tags;
    }

    public boolean save(Context context){
        return DatabaseManager.getInstance(context).insertChannel(this);
    }

    public static ArrayList<Channel> getChannels(Context context,
                                                 Listeners.OnGetChannelsListener listener)
    {
        ArrayList<Channel> saved = DatabaseManager.getInstance(context).getChannels();
        NetworkingManager.getInstance(context).getChannels(saved, listener);
        return saved;
    }

    public static Channel getChannel(Context context, int serverId) {
        return DatabaseManager.getInstance(context).getChannel(serverId);
    }

    public static void createChannel(Context context, User admin, String name,
                                     String categories, Listeners.OnGetNewChannelListener listener)
    {
        NetworkingManager.getInstance(context).createChannel(admin, name, categories, listener);
    }
}
