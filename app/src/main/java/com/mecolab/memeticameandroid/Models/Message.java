package com.mecolab.memeticameandroid.Models;


import android.content.Context;

import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.Networking.NetworkingManager;
import com.mecolab.memeticameandroid.Persistence.DatabaseManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class Message {
    public enum MessageType{
        TEXT, IMAGE, VIDEO, AUDIO, OTHER, NOT_SET, MMA
    }

    private static String[] ImageMimeTypes = { "image/jpeg", "image/jpg", "image/png" };
    private static String[] VideoMimeTypes = { "video/x-mpeg", "video/3gp", "video/3gpp",
            "video/quicktime", "video/mp4" };
    private static String[] AudioMimeTypes = { "audio/mpeg3", "audio/x-mpeg-3", "audio/mpeg",
            "audio/mp3","audio/mp4","audio/ogg", "audio/wav", "audio/3gp"};
    private static String[] MMAMimeTypes={"memeaudio/so"};

    private static String TextMimeType = "plain/text";

    public static String NoContent = "NO CONTEXT";

    public int mId;
    public final int mServerId;
    public final String mSender;
    public final String mContent;
    public final MessageType mType;
    public final String mMimeType;
    public final int mConversationId;
    public final String mDate;
    public final String url;
    // for channel memes
    public double mRating;
    public final String mTags;
    public final int mChannelId;
    public final int mSize;

    public int mProgress = 0;

    protected Message(int id, int serverId, String sender, String content, MessageType type,
                      String mime_type, int conversationId, String date, String url, int size, double rating,
                      String tags, int channelId) {
        mId = id;
        mServerId = serverId;
        mSender = sender;
        mContent = content;
        mType = type;
        mMimeType = mime_type;
        mConversationId = conversationId;
        mDate = date;
        mSize = size;
        this.url =url;
        mRating = rating;
        mTags = tags;
        mChannelId = channelId;

    }

    public static class Builder {
        private int mId = -1;
        private int mServerId = -1;
        private MessageType mType = MessageType.NOT_SET;
        private String mMimeType = "plain/text";
        private String mDate = "";
        private String mContent = "";
        private String mSender = "";
        private int mConversationId = -1;
        private String url;
        private double mRating = 0;
        private String mTags = "";
        private int mChannelId = -1;
        private int mSize = -1;

        public Message build() {
            if(mDate.equals("")) {
                /*TODO: */
                mDate = "";
            }
            if((mConversationId == -1 && mChannelId == -1) || mType == MessageType.NOT_SET)
                return null;
            return new Message(mId, mServerId, mSender, mContent, mType, mMimeType,
                        mConversationId, mDate, url, mSize, mRating, mTags, mChannelId);
        }

        public Builder setContent(String content) {
            mContent = content;
            return this;
        }
        public Builder setUrl(String url){
            this.url= url;
            return this;
        }

        public Builder setChannelId(int channelId){
            mChannelId = channelId;
            return this;
        }

        public Builder setRating(double rating){
            mRating= rating;
            return this;
        }

        public Builder setTags(String tags){
            mTags = tags;
            return this;
        }

        public Builder setSender(String sender) {
            mSender = sender;
            return this;
        }

        public Builder setConversationId(int conversationId) {
            mConversationId = conversationId;
            return this;
        }

        public Builder setId(int id) {
            mId = id;
            return this;
        }

        public Builder setServerId(int serverId) {
            mServerId = serverId;
            return this;
        }

        public Builder setMimeType(String mimeType) {
            mMimeType = mimeType;
            if (Arrays.asList(ImageMimeTypes).contains(mMimeType)) {
                mType = MessageType.IMAGE;
            }
            else if (Arrays.asList(VideoMimeTypes).contains(mMimeType)) {
                mType = MessageType.VIDEO;
            }
            else if (Arrays.asList(AudioMimeTypes).contains(mMimeType)) {
                mType = MessageType.AUDIO;
            }
            else if(Arrays.asList(MMAMimeTypes).contains(mMimeType)){
                mType = MessageType.MMA;
            }
            else if (TextMimeType.equals(mimeType)) {
                mType = MessageType.TEXT;
            }

            else {
                mType = MessageType.OTHER;
            }
            return this;
        }

        public Builder setDate(String date) {
            mDate = date;
            return this;
        }

        public Builder setSize(int size) {
            mSize = size;
            return this;
        }
    }

    public static ArrayList<Message> getMessagesFromChannel(Context context, Channel channel,
                                                            Listeners.OnGetMemesListener listener){
        ArrayList<Message> savedMemes = DatabaseManager.getInstance(context).getMessagesForChannel(channel.mServerId);
        int index = savedMemes.size() - 1;
        int id = -1;
        if (index > 0) {
            id = savedMemes.get(index).mServerId;
        }
        NetworkingManager.getInstance(context).getMemesFromChannel(channel, savedMemes, listener);
        return savedMemes;
    }

    public void rateMeme(Context context, float rating, Listeners.OnRateMemeListener listener){
        NetworkingManager.getInstance(context).rateMeme(User.getLoggedUser(context), this, rating, listener);
    }

    public static ArrayList<Message> getMessages(Context context, Conversation conversation,
                                                 Listeners.OnGetMessagesListener listener){
        ArrayList<Message> savedMessages = DatabaseManager.getInstance(context).getMessages(conversation.mServerId);
        int index = savedMessages.size() - 1;
        int id = -1;
        if (index > 0) {
            id = savedMessages.get(index).mServerId;
        }
        NetworkingManager.getInstance(context).getMessages(conversation.mServerId,
                id, savedMessages, listener);
        return savedMessages;
    }

    public void sendMessage(Context context, Listeners.SendMessageListener listener) {
        NetworkingManager.getInstance(context).sendMessage(this, listener);
    }

    public void sendMemeToChannel(Context context, Listeners.SendMessageListener listener) {
        NetworkingManager.getInstance(context).sendMemeToChannel(this, listener);
    }

    public boolean save(Context context){
        long result = DatabaseManager.getInstance(context).insertMessage(this);
        return result > 0;
    }

    public boolean update(Context context){
        long result = DatabaseManager.getInstance(context).updateMessage(this);
        return result > 0;
    }

    public static Message getMessage(Context context, int serverId) {
        return DatabaseManager.getInstance(context).getMessage(serverId);
    }

    public void setRating(double rating){
        mRating = rating;
    }
}
