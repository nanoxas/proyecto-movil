package com.mecolab.memeticameandroid.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.etiennelawlor.imagegallery.library.activities.ImageGalleryActivity;
import com.mecolab.memeticameandroid.Utils.Vars;
import com.mecolab.memeticameandroid.Fragments.ConversationFragment;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.R;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;

public class ConversationActivity extends AppCompatActivity implements
        EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener {
    public static final int GENERIC_FILE_CODE = 0;
    public static final int PICK_IMAGE_CODE = 1;
    public static final int TEXT_MEME_CODE = 2;
    public static final int PICK_MEME_IMAGE_CODE = 3;
    public static final int PICK_MEME_AUDIO_CODE = 4;
    public static final int PICK_CONTACT_CODE = 5;
    public static final int PICK_PUBLIC_MEME = 6;
    public static final int REQUEST_VIDEO = 10;
    public static final int REQUEST_CAMERA_PERMISSION = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        User me = User.getLoggedUser(this);
        Vars.phoneNumber = me.mPhoneNumber;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        return true;
    }

    public void addMeme(int resid) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), resid);
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString() + "/MemeticaMe";
        File file = new File(extStorageDirectory, getResources().getResourceEntryName(resid) + ".png");
        if (!file.exists()) {
            FileOutputStream outStream = null;
            try {
                outStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            try {
                outStream.flush();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.gallery) {
            addMeme(R.drawable.ancient_aliens);
            addMeme(R.drawable.bien_guagua);
            addMeme(R.drawable.conspiracy);
            addMeme(R.drawable.futurama_fry);
            addMeme(R.drawable.mina_tonta);
            addMeme(R.drawable.nino_africano_feliz);
            addMeme(R.drawable.one_does_not_simply);
            addMeme(R.drawable.tell_me_more);
            addMeme(R.drawable.tom_cruise_laughing);
            addMeme(R.drawable.toy_story);
            addMeme(R.drawable.true_story);

            Intent intent = new Intent(this, ImageGalleryActivity.class);
            File dir = new File("/storage/emulated/0/MemeticaMe");
            File[] listOfFiles = dir.listFiles();
            ArrayList<String> images = new ArrayList<>();
            for (int i = 0; i < listOfFiles.length; i++) {
                String mimeType = URLConnection.guessContentTypeFromName(listOfFiles[i].getName());
                if (mimeType != null) {
                    if (listOfFiles[i].isFile() && mimeType.contains("image")) {

                        images.add(listOfFiles[i].getAbsolutePath());

                    }
                }
            }


            Bundle bundle = new Bundle();
            bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, images);
            bundle.putString(ImageGalleryActivity.KEY_TITLE, "Gallery");
            intent.putExtras(bundle);

            startActivity(intent);

        } else if (id == R.id.action_send_photo) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_CODE);
            return true;

        } else if (id == R.id.action_send_video) {
            showVideoMenu();//Ofrecer ver galeria o grabar video
            return true;

        } else if (id == R.id.action_send_generic_file) {
            // This always works
            Intent i = new Intent(this, FilePickerActivity.class);
            // This works if you defined the intent filter
            // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

            // Set these depending on your use case. These are the defaults.
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
            i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

            // Configure initial directory by specifying a String.
            // You could specify a String like "/storage/emulated/0/", but that can
            // dangerous. Always use Android's API calls to get paths to the SD-card or
            // internal memory.
            i.putExtra(FilePickerActivity.EXTRA_START_PATH,
                    Environment.getExternalStorageDirectory().getPath());

            startActivityForResult(i, GENERIC_FILE_CODE);
            return true;
        } else if (id == R.id.action_invite_contact) {
            ConversationFragment conversationFragment =
                    (ConversationFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.ChatActivity_ConversationFragment);

            if (!conversationFragment.isGroupConversation()) {
                Toast.makeText(this, "This isn't a group conversation.", Toast.LENGTH_SHORT).show();
                return true;
            }
            Intent intent = new Intent(this, ContactsActivity.class);
            startActivityForResult(intent, PICK_CONTACT_CODE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("canvas act onActRes", "requestCode:" + "requestCode");// 2

        if (requestCode == GENERIC_FILE_CODE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(String.valueOf(uri));
            if (extension.equals("mma")) {
                type = "memeaudio/so";
            } else if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }


            ConversationFragment conversationFragment =
                    (ConversationFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.ChatActivity_ConversationFragment);
            conversationFragment.sendFileMessage(uri, type);
        } else if (requestCode == PICK_IMAGE_CODE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String mimeType = getContentResolver().getType(uri);
            ConversationFragment conversationFragment =
                    (ConversationFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.ChatActivity_ConversationFragment);
            conversationFragment.sendFileMessage(uri, mimeType);
        } else if (requestCode == PICK_CONTACT_CODE && resultCode == RESULT_OK) {
            String number = data.getStringExtra(User.PHONE_NUMBER);

            ConversationFragment conversationFragment =
                    (ConversationFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.ChatActivity_ConversationFragment);

            boolean exists = conversationFragment.addParticipant(number, new Listeners.OnUserAddedListener() {
                @Override
                public void onUserAdded(boolean successful) {
                    if (successful) {
                        Toast.makeText(ConversationActivity.this,
                                "Contact added successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ConversationActivity.this,
                                "Connection failed", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            if (!exists) {
                Toast.makeText(ConversationActivity.this,
                        "Contact already in group", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_VIDEO && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String mimeType = getContentResolver().getType(uri);
            if (mimeType.contains("audio"))
                mimeType = mimeType.replace("audio", "video");
            if (mimeType.contains("3gpp"))
                mimeType = mimeType.replace("3gpp", "3gp");
            ConversationFragment conversationFragment =
                    (ConversationFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.ChatActivity_ConversationFragment);
            conversationFragment.sendFileMessage(uri, mimeType);
        }
    }

    @Override
    public void onEmojiconBackspaceClicked(View view) {
        ConversationFragment conversationFragment =
                (ConversationFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.ChatActivity_ConversationFragment);
        conversationFragment.onEmojiconBackspaceClicked(view);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        ConversationFragment conversationFragment =
                (ConversationFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.ChatActivity_ConversationFragment);
        conversationFragment.onEmojiconClicked(emojicon);
    }

    private void showVideoMenu() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(new String[]
                {
                        getString(R.string.search_video), getString(R.string.take_video)
                }, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) { //Search vid
                    dispatchSelectVideoIntent();
                } else if (which == 1) { // Take vid, then send
                    dispatchTakeVideoIntent();
                }
            }
        });
        builder.show();
    }

    private void dispatchSelectVideoIntent() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("video/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("video/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Video");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        startActivityForResult(chooserIntent, REQUEST_VIDEO);
    }

    private void dispatchTakeVideoIntent() {

        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_PERMISSION);
            }
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        }
    }

}
