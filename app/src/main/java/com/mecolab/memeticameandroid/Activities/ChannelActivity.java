package com.mecolab.memeticameandroid.Activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.etiennelawlor.imagegallery.library.activities.ImageGalleryActivity;
import com.mecolab.memeticameandroid.Fragments.ChannelFragment;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Utils.Vars;
import com.nononsenseapps.filepicker.FilePickerActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;

public class ChannelActivity extends AppCompatActivity {

    public static final int GENERIC_FILE_CODE = 0;
    public static final int PICK_IMAGE_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);
        User me = User.getLoggedUser(this);
        Vars.phoneNumber = me.mPhoneNumber;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_channel, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ChannelFragment channelFragment =
                        (ChannelFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.ChannelActivity_ChannelFragment);
                channelFragment.filterMemes(newText);
                return true;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                ChannelFragment channelFragment =
                        (ChannelFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.ChannelActivity_ChannelFragment);
                channelFragment.filterMemes("");
                return false;
            }
        });
        return true;
    }

    public void addMeme(int resid) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), resid);
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString() + "/MemeticaMe";
        File file = new File(extStorageDirectory, getResources().getResourceEntryName(resid) + ".png");
        if (!file.exists()) {
            FileOutputStream outStream = null;
            try {
                outStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            try {
                outStream.flush();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.gallery) {
            addMeme(R.drawable.ancient_aliens);
            addMeme(R.drawable.bien_guagua);
            addMeme(R.drawable.conspiracy);
            addMeme(R.drawable.futurama_fry);
            addMeme(R.drawable.mina_tonta);
            addMeme(R.drawable.nino_africano_feliz);
            addMeme(R.drawable.one_does_not_simply);
            addMeme(R.drawable.tell_me_more);
            addMeme(R.drawable.tom_cruise_laughing);
            addMeme(R.drawable.toy_story);
            addMeme(R.drawable.true_story);

            Intent intent = new Intent(this, ImageGalleryActivity.class);
            File dir = new File("/storage/emulated/0/MemeticaMe");
            File[] listOfFiles = dir.listFiles();
            ArrayList<String> images = new ArrayList<>();
            for (int i = 0; i < listOfFiles.length; i++) {
                String mimeType = URLConnection.guessContentTypeFromName(listOfFiles[i].getName());
                if (mimeType != null) {
                    if (listOfFiles[i].isFile() && mimeType.contains("image")) {

                        images.add(listOfFiles[i].getAbsolutePath());

                    }
                }
            }


            Bundle bundle = new Bundle();
            bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, images);
            bundle.putString(ImageGalleryActivity.KEY_TITLE, "Gallery");
            intent.putExtras(bundle);

            startActivity(intent);

        } else if (id == R.id.action_send_photo) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_CODE);
            return true;
        } else if (id == R.id.action_send_generic_file) {
            // This always works
            Intent i = new Intent(this, FilePickerActivity.class);
            // This works if you defined the intent filter
            // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

            // Set these depending on your use case. These are the defaults.
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
            i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

            // Configure initial directory by specifying a String.
            // You could specify a String like "/storage/emulated/0/", but that can
            // dangerous. Always use Android's API calls to get paths to the SD-card or
            // internal memory.
            i.putExtra(FilePickerActivity.EXTRA_START_PATH,
                    Environment.getExternalStorageDirectory().getPath());

            startActivityForResult(i, GENERIC_FILE_CODE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("canvas act onActRes", "requestCode:" + "requestCode");// 2

        if (requestCode == GENERIC_FILE_CODE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(String.valueOf(uri));
            if (extension.equals("mma")) {
                type = "memeaudio/so";

                ChannelFragment channelFragment =
                        (ChannelFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.ChannelActivity_ChannelFragment);
                channelFragment.sendFileMessage(uri, type);
            } else if (extension != null) {
                Toast.makeText(this, "This is not a meme",
                        Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == PICK_IMAGE_CODE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String mimeType = getContentResolver().getType(uri);
            ChannelFragment channelFragment =
                    (ChannelFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.ChannelActivity_ChannelFragment);
            channelFragment.sendFileMessage(uri, mimeType);
        }
    }
}
