package com.mecolab.memeticameandroid.Activities;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.media.MediaRouteProvider;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.etiennelawlor.imagegallery.library.activities.FullScreenImageGalleryActivity;
import com.etiennelawlor.imagegallery.library.activities.ImageGalleryActivity;
import com.etiennelawlor.imagegallery.library.adapters.FullScreenImageGalleryAdapter;
import com.etiennelawlor.imagegallery.library.adapters.ImageGalleryAdapter;
import com.etiennelawlor.imagegallery.library.enums.PaletteColorType;
import com.mecolab.memeticameandroid.Fragments.ChannelsFragment;
import com.mecolab.memeticameandroid.Fragments.ContactsFragment;
import com.mecolab.memeticameandroid.Fragments.ConversationsFragment;
import com.mecolab.memeticameandroid.Fragments.InvitationFragment;
import com.mecolab.memeticameandroid.GCM.RegistrationIntentService;
import com.mecolab.memeticameandroid.Models.Channel;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Utils.MediaPlayerUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.squareup.picasso.Callback;


import static com.etiennelawlor.imagegallery.library.enums.PaletteColorType.DARK_MUTED;
import static com.etiennelawlor.imagegallery.library.enums.PaletteColorType.DARK_VIBRANT;
import static com.etiennelawlor.imagegallery.library.enums.PaletteColorType.LIGHT_MUTED;
import static com.etiennelawlor.imagegallery.library.enums.PaletteColorType.LIGHT_VIBRANT;
import static com.etiennelawlor.imagegallery.library.enums.PaletteColorType.MUTED;
import static com.etiennelawlor.imagegallery.library.enums.PaletteColorType.VIBRANT;


public class MainActivity extends AppCompatActivity implements
        ContactsFragment.OnContactSelectedListener,
        ConversationsFragment.OnConversationSelectedListener,
        ChannelsFragment.OnChannelSelectedListener,
        ImageGalleryAdapter.ImageThumbnailLoader,
        FullScreenImageGalleryAdapter.FullScreenImageLoader {

    public static final int ADD_CONTACT_REQUEST = 0;
    private PaletteColorType paletteColorType;
    private Menu memeAudioMenu;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    private boolean hasntContactsPermissions() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageGalleryActivity.setImageThumbnailLoader(this);
        FullScreenImageGalleryActivity.setFullScreenImageLoader(this);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 103);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && hasntContactsPermissions()) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 101);
            // After this point you wait for callback in
            // onRequestPermissionsResult(int, String[], int[]) overriden method
        }
        User loggedUser = User.getLoggedUser(this);
        if (loggedUser == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        final ActionBar actionBar = getSupportActionBar();
        // Specify that tabs should be displayed in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        if (position < actionBar.getTabCount()) {
                            actionBar.setSelectedNavigationItem(position);
                        }
                    }
                });

        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // hide the given tab
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // probably ignore this event
            }
        };

        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.MainActivity_TabConversations)
                        .setTabListener(tabListener));

        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.MainActivity_TabContacts)
                        .setTabListener(tabListener));

        actionBar.addTab(actionBar.newTab()
                .setText(R.string.MainActivity_TabChannels)
                .setTabListener(tabListener));

        actionBar.addTab(actionBar.newTab()
                .setText(R.string.MainActivity_TabInvites)
                .setTabListener(tabListener));

        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);

        declineInvitationIfNecessary();
        openTabIfNecessary();
    }

    @Override
    public void loadImageThumbnail(ImageView iv, String imageUrl, int dimension) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(iv.getContext())
                    .load("file://" + imageUrl)
                    .resize(dimension, dimension)
                    .centerCrop()
                    .into(iv);
        } else {
            iv.setImageDrawable(null);
        }
    }

    @Override
    public void loadFullScreenImage(final ImageView iv, final String imageUrl, int width, final LinearLayout bgLinearLayout) {
        if (!TextUtils.isEmpty(imageUrl)) {

            if (imageUrl.contains("Unzipped MMA")) {
                FrameLayout parent = (FrameLayout) iv.getParent();
                (parent.findViewById(R.id.buttons)).bringToFront();

                final ImageButton stopButton = (ImageButton) parent.findViewById(R.id.stopButtonGallery);
                stopButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_stop, getTheme()));
                stopButton.setVisibility(View.VISIBLE);

                final ImageButton playButton = (ImageButton) parent.findViewById(R.id.playButtonGallery);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_dark, getTheme()));
                playButton.setVisibility(View.VISIBLE);

                File f = new File(imageUrl);
                String folder = f.getParent();
                File parentFold = new File(folder);
                File[] listOfFiles = parentFold.listFiles();

                for (int i = 0; i < listOfFiles.length; i++) {
                    String mimeType = URLConnection.guessContentTypeFromName(listOfFiles[i].getName());
                    if (mimeType != null) {
                        if (listOfFiles[i].isFile() && !mimeType.contains("image")) {

                            String url = "file://" + listOfFiles[i].getAbsolutePath(); // your URL here
                            final MediaPlayer mediaPlayer = new MediaPlayer();
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            try {
                                mediaPlayer.setDataSource(url);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            MediaPlayerUtils.initiatePlayer(mediaPlayer, playButton, stopButton);
                        }
                    }
                }
            }

            Picasso.with(iv.getContext())
                    .load("file://" + imageUrl)
                    .resize(width, 0)
                    .into(iv, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
                            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                                public void onGenerated(Palette palette) {
                                    applyPalette(palette, bgLinearLayout);
                                }
                            });
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            iv.setImageDrawable(null);
        }
    }
    // endregion

    // region Helper Methods
    private void applyPalette(Palette palette, LinearLayout bgLinearLayout) {
        int bgColor = getBackgroundColor(palette);
        if (bgColor != -1)
            bgLinearLayout.setBackgroundColor(bgColor);
    }

    private int getBackgroundColor(Palette palette) {
        int bgColor = -1;

        int vibrantColor = palette.getVibrantColor(0x000000);
        int lightVibrantColor = palette.getLightVibrantColor(0x000000);
        int darkVibrantColor = palette.getDarkVibrantColor(0x000000);

        int mutedColor = palette.getMutedColor(0x000000);
        int lightMutedColor = palette.getLightMutedColor(0x000000);
        int darkMutedColor = palette.getDarkMutedColor(0x000000);

        if (paletteColorType != null) {
            switch (paletteColorType) {
                case VIBRANT:
                    if (vibrantColor != 0) { // primary option
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) { // fallback options
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    } else if (mutedColor != 0) {
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    }
                    break;
                case LIGHT_VIBRANT:
                    if (lightVibrantColor != 0) { // primary option
                        bgColor = lightVibrantColor;
                    } else if (vibrantColor != 0) { // fallback options
                        bgColor = vibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    } else if (mutedColor != 0) {
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    }
                    break;
                case DARK_VIBRANT:
                    if (darkVibrantColor != 0) { // primary option
                        bgColor = darkVibrantColor;
                    } else if (vibrantColor != 0) { // fallback options
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (mutedColor != 0) {
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    }
                    break;
                case MUTED:
                    if (mutedColor != 0) { // primary option
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) { // fallback options
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    } else if (vibrantColor != 0) {
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    }
                    break;
                case LIGHT_MUTED:
                    if (lightMutedColor != 0) { // primary option
                        bgColor = lightMutedColor;
                    } else if (mutedColor != 0) { // fallback options
                        bgColor = mutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    } else if (vibrantColor != 0) {
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    }
                    break;
                case DARK_MUTED:
                    if (darkMutedColor != 0) { // primary option
                        bgColor = darkMutedColor;
                    } else if (mutedColor != 0) { // fallback options
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (vibrantColor != 0) {
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    }
                    break;
                default:
                    break;
            }
        }

        return bgColor;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void addMeme(int resid) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), resid);
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString() + "/MemeticaMe";
        File file = new File(extStorageDirectory, getResources().getResourceEntryName(resid) + ".png");
        if (!file.exists()) {
            FileOutputStream outStream = null;
            try {
                outStream = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void declineInvitationIfNecessary() {
        int acceptInvitationId = getIntent().getIntExtra("decline_invitation", -1);
        int notificationId = getIntent().getIntExtra("notification_id", -1);
        if (acceptInvitationId != -1) {
            Invitation invitation = Invitation.getInvitation(this, acceptInvitationId);
            if (invitation != null)
                invitation.accept(this);

            NotificationManager ntf_mgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            ntf_mgr.cancel(notificationId);
        }
    }

    private void openTabIfNecessary() {
        int openTab = getIntent().getIntExtra("openTab", -1);
        if (openTab != -1)
            mViewPager.setCurrentItem(openTab);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_new_two_conversation) {
            Intent intent = new Intent(this, NewChatActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_refresh_contacts) {
            if (mViewPager != null) {
                Fragment contactsFragment = mSectionsPagerAdapter.
                        getRegisteredFragment(mViewPager.getCurrentItem());
                if (contactsFragment != null && contactsFragment.getClass().equals(ContactsFragment.class)) {
                    ((ContactsFragment) contactsFragment).refreshContacts();
                }
            }
            return true;
        } else if (id == R.id.add_contact) {
            Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
            intent.putExtra("finishActivityOnSaveCompleted", true); // Fix for 4.0.3 +
            startActivityForResult(intent, ADD_CONTACT_REQUEST);
        } else if (id == R.id.gallery) {


            addMeme(R.drawable.ancient_aliens);
            addMeme(R.drawable.bien_guagua);
            addMeme(R.drawable.conspiracy);
            addMeme(R.drawable.futurama_fry);
            addMeme(R.drawable.mina_tonta);
            addMeme(R.drawable.nino_africano_feliz);
            addMeme(R.drawable.one_does_not_simply);
            addMeme(R.drawable.tell_me_more);
            addMeme(R.drawable.tom_cruise_laughing);
            addMeme(R.drawable.toy_story);
            addMeme(R.drawable.true_story);

            Intent intent = new Intent(MainActivity.this, ImageGalleryActivity.class);
            File dir = new File("/storage/emulated/0/MemeticaMe");
            File[] listOfFiles = dir.listFiles();
            ArrayList<String> images = new ArrayList<>();
            for (int i = 0; i < listOfFiles.length; i++) {
                String mimeType = URLConnection.guessContentTypeFromName(listOfFiles[i].getName());
                if (mimeType != null) {
                    if (listOfFiles[i].isFile() && mimeType.contains("image")) {

                        images.add(listOfFiles[i].getAbsolutePath());

                    }
                }
            }
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, images);
            bundle.putString(ImageGalleryActivity.KEY_TITLE, "Gallery");
            intent.putExtras(bundle);
            startActivity(intent);

        } else if (id == R.id.memeaudio_gallery) {
            Intent intent = new Intent(MainActivity.this, ImageGalleryActivity.class);
            File dir = new File("/storage/emulated/0/MemeticaMe");
            File[] listOfFiles = dir.listFiles();
            ArrayList<String> images = new ArrayList<>();
            for (int i = 0; i < listOfFiles.length; i++) {
                String mimeType = URLConnection.guessContentTypeFromName(listOfFiles[i].getName());

                if (listOfFiles[i].isDirectory() && listOfFiles[i].getAbsolutePath().contains("Unzipped MMA")) {

                    File[] meme = listOfFiles[i].listFiles();
                    for (File f : meme) {
                        String mtype = URLConnection.guessContentTypeFromName(f.getName());
                        if (mtype != null) {
                            if (f.isFile() && mtype.contains("image")) {
                                images.add(f.getAbsolutePath());
                            }
                        }
                    }
                }
            }
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, images);
            bundle.putString(ImageGalleryActivity.KEY_TITLE, "Memeaudios");
            intent.putExtras(bundle);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContactSelected(User contact) {
        Conversation conversation =
                Conversation.getTwoConversation(this, User.getLoggedUser(this), contact);

        if (conversation == null) {
            Conversation.createNewTwoConversation(this, User.getLoggedUser(this), contact,
                    new Listeners.OnGetNewTwoConversationListener() {
                        @Override
                        public void onConversationReceived(final Conversation conversation) {
                            if (conversation == null) {
                                Toast.makeText(getApplicationContext(), "connection failed",
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            conversation.save(MainActivity.this);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(getApplicationContext(),
                                            ConversationActivity.class);
                                    intent.putExtra(Conversation.SERVER_ID, conversation.mServerId);
                                    startActivity(intent);
                                }
                            });

                        }
                    });
        } else {
            Intent intent = new Intent(getApplicationContext(),
                    ConversationActivity.class);
            intent.putExtra(Conversation.SERVER_ID, conversation.mServerId);
            startActivity(intent);
        }
    }

    @Override
    public void onConversationSelected(Conversation conversation) {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendTextMessage(intent, conversation);
            } else if (type.startsWith("image/") || type.startsWith("audio/") || type.startsWith("application/")) {
                handleSendFileMessage(intent, conversation);
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/") || type.startsWith("audio/") || type.startsWith("application/")) {
                handleSendMultipleFileMessage(intent, conversation);
            }
        }

        // Go to selected activity
        Intent conversationIntent = new Intent(this, ConversationActivity.class);
        conversationIntent.putExtra(Conversation.SERVER_ID, conversation.mServerId);
        startActivity(conversationIntent);
    }

    @Override
    public void onChannelSelected(Channel channel) {
        Intent channelIntent = new Intent(this, ChannelActivity.class);
        channelIntent.putExtra(Channel.SERVER_ID, channel.mServerId);
        startActivity(channelIntent);
    }

    private void handleSendTextMessage(Intent intent, Conversation conversation) {
        String content = intent.getStringExtra(Intent.EXTRA_TEXT);
        sendMessage(content, conversation);
    }

    private void handleSendFileMessage(Intent intent, Conversation conversation) {
        String type = intent.getType();
        Uri fileUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        sendFileMessage(fileUri, type, conversation);
    }

    private void handleSendMultipleFileMessage(Intent intent, Conversation conversation) {
        ArrayList<Uri> uriArrayList = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (uriArrayList != null) {
            for (Uri fileUri : uriArrayList) {
                String type;
                String extension = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString());
                if (extension != null) {
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    type = mime.getMimeTypeFromExtension(extension);
                    sendFileMessage(fileUri, type, conversation);
                }
            }
        }
    }

    private void sendMessage(String content, Conversation conversation) {
        if (conversation == null) {
            Toast.makeText(this,
                    "Error selecting the conversation.",
                    Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        Message.Builder builder = new Message.Builder();
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        String stringDate = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);
        Message message = builder.setContent(content)
                .setSender(User.getLoggedUser(this).mPhoneNumber)
                .setMimeType("plain/text")
                .setConversationId(conversation.mServerId)
                .setDate(stringDate)
                .build();
        if (message.mContent.equals(""))
            return;
        message.sendMessage(this, new Listeners.SendMessageListener() {
            @Override
            public void onMessageSent(final Message msg) {

            }
        });
    }

    public void sendFileMessage(Uri uri, String mimeType, Conversation conversation) {
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        String stringDate = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);
        Message.Builder builder = new Message.Builder();
        Message message = builder
                .setContent(uri.toString())
                .setConversationId(conversation.mServerId)
                .setDate(stringDate)
                .setMimeType(mimeType)
                .setSender(User.getLoggedUser(this).mPhoneNumber)
                .build();
        message.sendMessage(this, new Listeners.SendMessageListener() {
            @Override
            public void onMessageSent(Message msg) {

            }
        });
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch (position) {
                case 0:
                    return ConversationsFragment.newInstance(position + 1);
                case 1:
                    return ContactsFragment.newInstance(position + 1);
                case 3:
                    return InvitationFragment.newInstance(position + 1);
                case 2:
                    return ChannelsFragment.newInstance(position + 1);
                default:
                    return null;
            }
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }


        @Override
        public int getCount() {
            // Show 2 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.MainActivity_TabConversations).toUpperCase(l);
                case 1:
                    return getString(R.string.MainActivity_TabContacts).toUpperCase(l);
                case 3:
                    return getString(R.string.MainActivity_TabInvites).toUpperCase(l);
                case 2:
                    return getString(R.string.MainActivity_TabChannels).toUpperCase(l);
            }
            return null;
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
