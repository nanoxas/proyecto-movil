package com.mecolab.memeticameandroid.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mecolab.memeticameandroid.Activities.ChannelActivity;
import com.mecolab.memeticameandroid.Activities.ContactsActivity;
import com.mecolab.memeticameandroid.Models.Channel;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Views.UserAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Mecolab on 04-12-2016.
 */

public class NewChannelFragment extends Fragment {

    @Bind(R.id.NewChannelFragment_ChannelNameView)
    EditText mNameView;

    @Bind(R.id.NewChannelFragment_ChannelCategoriesView)
    EditText mCategoriesView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_channel, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public void createChannel() {
        Channel.createChannel(getActivity(), User.getLoggedUser(getContext()),
                mNameView.getText().toString(), mCategoriesView.getText().toString(),
                new Listeners.OnGetNewChannelListener() {
                    @Override
                    public void onChannelReceived(Channel channel) {
                        Intent intent = new Intent(getActivity(), ChannelActivity.class);
                        intent.putExtra(Channel.SERVER_ID, channel.mServerId);
                        startActivity(intent);
                    }
                });

    }

}
