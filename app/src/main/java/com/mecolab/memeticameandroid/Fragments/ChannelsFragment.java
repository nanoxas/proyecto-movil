package com.mecolab.memeticameandroid.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mecolab.memeticameandroid.MemeticameApplication;
import com.mecolab.memeticameandroid.Models.Channel;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Views.ChannelAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChannelsFragment.OnChannelSelectedListener} interface
 * to handle interaction events.
 * Use the {@link ChannelsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChannelsFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";

    MessageReceiver messageReceiver;

    @Bind(R.id.ChannelsFragment_ChannelsView)
    ListView mChannelsView;

    private ChannelAdapter mAdapter;
    private ArrayList<Channel> mChannels;
    private User mLoggedUser;

    private OnChannelSelectedListener mListener;

    public ChannelsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sectionNumber Section number.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChannelsFragment newInstance(int sectionNumber) {
        ChannelsFragment fragment = new ChannelsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    Listeners.OnGetChannelsListener mOnGetChannelsListener =
            new Listeners.OnGetChannelsListener() {
                @Override
                public void onChannelsReceived(ArrayList<Channel> channels) {
                    for (Channel channel : channels){
                        if(mChannels.size() == 0){
                            mChannels.add(channel);
                        } else {
                            mChannels.add(0, channel);
                        }
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                }
            };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_channels, container, false);
        ButterKnife.bind(this, view);
        mLoggedUser = User.getLoggedUser(getContext());
        setChannels();
        setViews();
        setListeners();
        return view;
    }

    @Override
    public void onResume() {
        messageReceiver = new MessageReceiver();
        getActivity().registerReceiver(messageReceiver,
                new IntentFilter(MemeticameApplication.MESSAGE_RECEIVED_ACTION));
        super.onResume();
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(messageReceiver);
        super.onPause();
    }

    public class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MemeticameApplication.MESSAGE_RECEIVED_ACTION)) {
                final Message message = Message.getMessage(context,
                        intent.getIntExtra("SERVER_ID", -1));
                for (int i = 0; i < mChannels.size(); i++) {
                    if (message.mChannelId == mChannels.get(i).mServerId) {
                        //mChannels.get(i).mHasNewMessage = true;
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    private void setChannels() {
        mChannels = Channel.getChannels(getContext(), mOnGetChannelsListener);
    }

    private void setViews() {
        mAdapter = new ChannelAdapter(getActivity(), R.layout.channel_list_item,
                mChannels);
        mChannelsView.setAdapter(mAdapter);
    }

    private void setListeners() {
        mChannelsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //mChannels.get(position).mHasNewMessage = false;
                mAdapter.notifyDataSetChanged();
                mListener.onChannelSelected(mChannels.get(position));
            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Channel channel) {
        if (mListener != null) {
            mListener.onChannelSelected(channel);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChannelSelectedListener) {
            mListener = (OnChannelSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnChannelSelectedListener {
        // TODO: Update argument type and name
        void onChannelSelected(Channel channel);
    }
}
