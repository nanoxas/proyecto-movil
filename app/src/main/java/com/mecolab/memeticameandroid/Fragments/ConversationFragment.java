package com.mecolab.memeticameandroid.Fragments;


import android.Manifest;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.mecolab.memeticameandroid.Utils.FileManager;
import com.mecolab.memeticameandroid.MemeticameApplication;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Views.MessageAdapter;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ConversationFragment extends Fragment {
    @Bind(R.id.ConversationFragment_MessagesView)
    ListView mMessagesView;
    @Bind(R.id.ConversationFragment_NewMessageView)
    EmojiconEditText mNewMessageView;
    @Bind(R.id.ConversationFragment_SendButton)
    ImageButton mSendButton;
    @Bind(R.id.emoticon_button)
    ImageButton mEmoticonButton;
    @Bind(R.id.emojicons)
    FrameLayout mEmoticonLayout;
    @Bind(R.id.recordButton)
    ImageButton mRecordButton;

    public static final String SHARED_TEXT = "shared_text";
    public static final String SHARED_IMAGE_URI = "shared_uri";
    public static final String SHARED_TYPE = "shared_type";
    public static final int PERMISSIONS_RECORD_AUDIO = 102;

    private ArrayList<Message> mMessages;
    private MessageAdapter mMessagesAdapter;
    private User mLoggedUser;
    private Conversation mConversation;
    MessageReceiver messageReceiver;
    private boolean mSent = false;
    boolean recording = false;
    private MediaRecorder myAudioRecorder;
    private File outputFile = null;

    private ActionMode mActionMode;

    public ConversationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversation, container, false);
        ButterKnife.bind(this, view);
        mLoggedUser = User.getLoggedUser(getActivity());
        setConversation();
        setViews();
        if (getActivity() == null) return view;
        getActivity().setTitle(mConversation.mTitle);
        setListeners();
        messageReceiver = new MessageReceiver();
        getActivity().registerReceiver(messageReceiver,
                new IntentFilter("com.mecolab.memeticameandroid.MESSAGE_RECEIVED_ACTION"));
        checkSharedMedia();
        registerForContextMenu(mNewMessageView);
        setrecord();

        acceptInvitationIfNecessary();
        return view;
    }

    private void checkSharedMedia() {
        Bundle extras = getActivity().getIntent().getExtras();
        String type = extras.getString(SHARED_TYPE);
        if (type != null && !mSent) {
            if (type.equals("text/plain")) {
                String text = extras.getString(SHARED_TEXT);
                mNewMessageView.setText(text);
                sendMessage();
            } else if (type.startsWith("image/")) {
                Uri uri = Uri.parse(extras.getString(SHARED_IMAGE_URI));
                sendFileMessage(uri, "image/jpeg");
            }
            mSent = true;
        }
    }

    private void acceptInvitationIfNecessary() {
        int acceptInvitationId = getActivity().getIntent().getIntExtra("accept_invitation", -1);
        int notificationId = getActivity().getIntent().getIntExtra("notification_id", -1);

        if (acceptInvitationId != -1) {
            Invitation invitation = Invitation.getInvitation(getActivity(), acceptInvitationId);
            if (invitation != null)
                invitation.accept(getContext());

            NotificationManager ntf_mgr = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            ntf_mgr.cancel(notificationId);
        }
    }

    @Override
    public void onPause() {
        try {
            getActivity().unregisterReceiver(messageReceiver);
        } catch (IllegalArgumentException e) {
            super.onPause();
        }
        super.onPause();
    }


    public void onEmojiconBackspaceClicked(View view) {
        EmojiconsFragment.backspace(mNewMessageView);

    }

    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(mNewMessageView, emojicon);

    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        if (mEmoticonLayout.getVisibility() == FrameLayout.GONE) {
            mEmoticonLayout.setVisibility(FrameLayout.VISIBLE);
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                    .commit();
        } else if (mEmoticonLayout.getVisibility() == FrameLayout.VISIBLE) {
            mEmoticonLayout.setVisibility(FrameLayout.GONE);
        }

    }

    public class MessageReceiver extends BroadcastReceiver {
        public MessageReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.mecolab.memeticameandroid.MESSAGE_RECEIVED_ACTION")) {
                addGcmMessage(intent);
                abortBroadcast();
            }
        }
    }

    private void addGcmMessage(Intent intent) {
        final Message message = Message.getMessage(getActivity(),
                intent.getIntExtra("SERVER_ID", -1));
        if (message.mConversationId == mConversation.mServerId) {
            mMessages.add(message);
            mMessagesAdapter.notifyDataSetChanged();
            mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);

        }
    }

    private void setViews() {
        mMessages = Message.getMessages(getActivity(), mConversation,
                new Listeners.OnGetMessagesListener() {
                    @Override
                    public void onMessagesReceived(final ArrayList<Message> messages) {
                        if (getActivity() == null) return;
                        Collections.sort(messages, new Comparator<Message>() {
                            @Override
                            public int compare(Message o1, Message o2) {
                                return o1.mServerId - o2.mServerId;
                            }
                        });
                        for (Message message : messages) {
                            if (message.save(getActivity())) {
                                mMessages.add(message);
                            }
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMessagesAdapter.notifyDataSetChanged();
                                mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);
                            }
                        });
                    }
                });
        mMessagesAdapter =
                new MessageAdapter(getActivity(), R.layout.message_text_list_item, mMessages);
        mMessagesView.setAdapter(mMessagesAdapter);
        mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);

        mMessagesView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int pos, long id) {

                Log.v("long clicked", "Message: " + mMessages.get(pos).mContent);

                if (mActionMode != null) {
                    return false;
                }

                // Start the CAB using the ActionMode.Callback defined above
                mActionMode = getActivity().startActionMode(mActionModeCallback);
                mActionMode.setTag(pos);
                view.setSelected(true);

                return true;
            }
        });
    }

    private void setConversation() {
        int conversationServerId = getActivity().getIntent().getIntExtra(Conversation.SERVER_ID, 0);
        mConversation = Conversation.getConversation(getActivity(), conversationServerId);
    }

    private void setListeners() {
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        mEmoticonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEmojiconFragment(false);
            }
        });
        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                record();
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setrecord();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;


            }
        }
    }

    private void record() {


        if (recording == false) {
            recording = true;

            mRecordButton.setImageResource(R.drawable.ic_stop);

            try {
                myAudioRecorder.prepare();
                myAudioRecorder.start();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Toast.makeText(getContext(), "Recording started", Toast.LENGTH_LONG).show();
        } else {
            recording = false;
            myAudioRecorder.stop();
            myAudioRecorder.release();
            myAudioRecorder = null;
            mRecordButton.setImageResource(R.drawable.ic_fiber_manual_record);

            File audiosrc = new File(outputFile.getAbsolutePath() + "/recording.3gp");
            try {
                byte[] bytes = FileUtils.readFileToByteArray(audiosrc);
                String encodedImage = Base64.encodeToString(bytes, Base64.DEFAULT);
                sendAudio(encodedImage);


            } catch (IOException e) {
                e.printStackTrace();
            }


            Toast.makeText(getContext(), "Audio recorded successfully", Toast.LENGTH_LONG).show();
        }


    }

    private void setrecord() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_RECORD_AUDIO);
        } else {
            myAudioRecorder = new MediaRecorder();
            myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            outputFile = this.getActivity().getExternalFilesDir(Environment.DIRECTORY_MUSIC);
            if (outputFile != null) {
                myAudioRecorder.setOutputFile(outputFile.getAbsolutePath() + "/recording.3gp");
            }
        }
    }

    private void sendAudio(String encode) {

        if (mConversation == null) {
            Toast.makeText(getActivity(),
                    "Wait a moment, the conversation is being created.",
                    Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        Message.Builder builder = new Message.Builder();
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        String stringDate = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);
        Message message = builder.setContent(encode)
                .setSender(mLoggedUser.mPhoneNumber)
                .setMimeType("audio/3gp")
                .setConversationId(mConversation.mServerId)
                .setDate(stringDate)
                .build();
        if (message.mContent.equals(""))
            return;
        if (message.mType != Message.MessageType.TEXT) {
            mMessages.add(message);
            mMessagesAdapter.notifyDataSetChanged();
        }
        message.sendMessage(getActivity(), new Listeners.SendMessageListener() {
            @Override
            public void onMessageSent(final Message msg) {
                if (msg.save(getActivity())) {
                    if (msg.mType == Message.MessageType.TEXT && getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMessages.add(msg);
                                mMessagesAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        });
        mNewMessageView.setText("");

    }


    private void sendMessage() {
        if (mConversation == null) {
            Toast.makeText(getActivity(),
                    "Wait a moment, the conversation is being created.",
                    Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        Message.Builder builder = new Message.Builder();
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        String stringDate = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);
        Message message = builder.setContent(mNewMessageView.getText().toString())
                .setSender(mLoggedUser.mPhoneNumber)
                .setMimeType("plain/text")
                .setConversationId(mConversation.mServerId)
                .setDate(stringDate)
                .build();
        if (message.mContent.equals(""))
            return;
        if (message.mType != Message.MessageType.TEXT) {
            mMessages.add(message);
            mMessagesAdapter.notifyDataSetChanged();
        }
        message.sendMessage(getActivity(), new Listeners.SendMessageListener() {
            @Override
            public void onMessageSent(final Message msg) {
                if (msg.save(getActivity())) {
                    if (msg.mType == Message.MessageType.TEXT && getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMessages.add(msg);
                                mMessagesAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        });
        mNewMessageView.setText("");
    }

    public void sendFileMessage(Uri uri, String mimeType) {
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        String stringDate = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);
        Message.Builder builder = new Message.Builder();
        Message message = builder
                .setContent(uri.toString())
                .setConversationId(mConversation.mServerId)
                .setDate(stringDate)
                .setMimeType(mimeType)
                .setSender(User.getLoggedUser(getActivity()).mPhoneNumber)
                .build();
        if (message.mType != Message.MessageType.TEXT) {
            mMessages.add(message);
            mMessagesAdapter.notifyDataSetChanged();
        }
        message.sendMessage(getActivity(), new Listeners.SendMessageListener() {
            @Override
            public void onMessageSent(Message msg) {
                if (msg.mType == Message.MessageType.TEXT && getActivity() != null
                        && msg.save(getActivity())) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mMessagesAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    public boolean isGroupConversation() {
        return mConversation.mIsGroup;
    }

    /*Ojo: si devuelve true quiere decir que se puede agregar, no qu se agrego*/
    public boolean addParticipant(String number, final Listeners.OnUserAddedListener listener) {
        User user = User.getUser(getActivity(), number);
        return mConversation.addParticipant(getActivity(), user, listener);
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_copy, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_copy:
                    Message msg = mMessages.get((int) mActionMode.getTag());
                    ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);

                    if (msg.mType == Message.MessageType.AUDIO) {
                        ClipData clip = ClipData.newUri(getContext().getContentResolver(), "AUDIO", Uri.parse(msg.mContent));
                        clipboard.setPrimaryClip(clip);
                    } else if (msg.mType == Message.MessageType.VIDEO) {
                            ClipData clip = ClipData.newUri(getContext().getContentResolver(), "VIDEO", Uri.parse(msg.mContent));
                            clipboard.setPrimaryClip(clip);
                    } else if (msg.mType != Message.MessageType.TEXT) {
                        //ClipData clip = ClipData.newPlainText("URI", msg.mContent);
                        ClipData clip = ClipData.newUri(getContext().getContentResolver(), "URI", Uri.parse(msg.mContent));
                        clipboard.setPrimaryClip(clip);
                    } else {
                        ClipData clip = ClipData.newPlainText("TEXT", msg.mContent);
                        clipboard.setPrimaryClip(clip);
                    }

                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_paste, menu);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle("Paste?");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_paste:
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);

                if (clipboard.hasPrimaryClip()) {

                    if (clipboard.getPrimaryClipDescription().getLabel().toString().equals("TEXT")) {
                        String pasteData = clipboard.getPrimaryClip().getItemAt(0).getText().toString();
                        mNewMessageView.setText(pasteData);
                    } else if (clipboard.getPrimaryClipDescription().getLabel().toString().equals("AUDIO")) {
                        Uri pasteUri = clipboard.getPrimaryClip().getItemAt(0).getUri();
                        try {
                            String encodedImage = FileManager.loadBase64(getContext(), pasteUri);
                            sendAudio(encodedImage);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if (clipboard.getPrimaryClipDescription().getLabel().toString().equals("VIDEO")) {
                        Uri pasteUri = clipboard.getPrimaryClip().getItemAt(0).getUri();
                        String path = pasteUri.getPath();
                        sendFileMessage(pasteUri, "video/" +
                                path.substring(path.lastIndexOf(".") + 1, path.length()) );
                    } else {
                        Uri pasteUri = clipboard.getPrimaryClip().getItemAt(0).getUri();
                        //String mimeType = FileManager.getMimeType(pasteData);

                        String mimeType = FileManager.getMimeType(pasteUri.toString());
                        if (mimeType != null)
                            sendFileMessage(pasteUri, mimeType);
                    }
                }

                break;
        }
        return true;
    }

}
