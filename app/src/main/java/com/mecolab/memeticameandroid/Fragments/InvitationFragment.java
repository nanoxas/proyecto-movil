package com.mecolab.memeticameandroid.Fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Views.InvitationAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvitationFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<Invitation> mInvitations = Invitation.getPendingInvitations(getContext());
    private InvitationAdapter mAdapter;
    private ListView inviteList;

    InvitationReceiver invitationReceiver;
    private IntentFilter mFilter = new IntentFilter("com.mecolab.memeticameandroid.INVITATION_RECEIVED_ACTION");

    public InvitationFragment() {
        // Required empty public constructor
    }

    public static InvitationFragment newInstance(int sectionNumber) {
        InvitationFragment fragment = new InvitationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_invitation, container, false);

        mInvitations = Invitation.getPendingInvitations(getContext());
        mAdapter = new InvitationAdapter(getContext(), R.layout.invitation_list_item, mInvitations);
        inviteList = (ListView) view.findViewById(R.id.invites_list);
        inviteList.setAdapter(mAdapter);

        invitationReceiver = new InvitationReceiver();
        getActivity().registerReceiver(invitationReceiver,
                new IntentFilter("com.mecolab.memeticameandroid.INVITATION_RECEIVED_ACTION"));

        return view;
    }

    @Override
    public void onPause() {
        try {
            getActivity().unregisterReceiver(invitationReceiver);
        } catch (IllegalArgumentException e) {
            super.onPause();
        }
        super.onPause();
    }

    public class InvitationReceiver extends BroadcastReceiver {
        public InvitationReceiver() { }
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.mecolab.memeticameandroid.INVITATION_RECEIVED_ACTION")) {
                addGcmInvitation(intent);
                abortBroadcast();
            }
        }
    }

    private void addGcmInvitation(Intent intent){
        final Invitation invitation = Invitation.getInvitation(getActivity(),
                intent.getStringExtra("user_phone"),
                intent.getIntExtra("conversation_id", -1));
        if (invitation != null) {
            mInvitations.add(invitation);
            mAdapter.notifyDataSetChanged();
            inviteList.setSelection(mAdapter.getCount() - 1);
        }
    }
}
