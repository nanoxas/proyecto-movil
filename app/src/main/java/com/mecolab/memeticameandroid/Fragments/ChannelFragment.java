package com.mecolab.memeticameandroid.Fragments;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.mecolab.memeticameandroid.Models.Channel;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.Listeners;
import com.mecolab.memeticameandroid.R;
import com.mecolab.memeticameandroid.Utils.FileManager;
import com.mecolab.memeticameandroid.Views.MessageAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChannelFragment extends Fragment {
    @Bind(R.id.ChannelFragment_MessagesView)
    ListView mMessagesView;

    public static final String SHARED_TEXT = "shared_text";
    public static final String SHARED_IMAGE_URI = "shared_uri";
    public static final String SHARED_TYPE = "shared_type";
    public static final int  PERMISSIONS_RECORD_AUDIO = 102;


    private ArrayList<Message> mMessages;
    private MessageAdapter mMessagesAdapter;
    private User mLoggedUser;
    private Channel mChannel;
    MessageReceiver messageReceiver;
    private boolean mSent = false;

    private ActionMode mActionMode;

    public ChannelFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channel, container, false);
        ButterKnife.bind(this, view);
        mLoggedUser = User.getLoggedUser(getActivity());
        setChannel();
        setViews();
        if (getActivity() == null) return view;
        getActivity().setTitle(mChannel.mName);
        messageReceiver = new MessageReceiver();
        getActivity().registerReceiver(messageReceiver,
                new IntentFilter("com.mecolab.memeticameandroid.MESSAGE_RECEIVED_ACTION"));
        checkSharedMedia();

        return view;
    }

    private void checkSharedMedia() {
        Bundle extras = getActivity().getIntent().getExtras();
        String type = extras.getString(SHARED_TYPE);
        if (type != null && !mSent) {
            if (type.startsWith("image/")) {
                Uri uri = Uri.parse(extras.getString(SHARED_IMAGE_URI));
                String tags = "";
                sendFileMessage(uri, "image/jpeg");
            }
            mSent = true;
        }
    }

    public void filterMemes(String searchQuery) {
        ArrayList<Message> unfilteredMessages = Message.getMessagesFromChannel(getActivity(), mChannel,
                new Listeners.OnGetMemesListener() {
                    @Override
                    public void onMemesReceived(final ArrayList<Message> messages) {
                        if (getActivity() == null) return;
                        Collections.sort(messages, new Comparator<Message>() {
                            @Override
                            public int compare(Message o1, Message o2) {
                                return o1.mServerId - o2.mServerId;
                            }
                        });
                        for (Message message : messages) {
                            if (message.save(getActivity())) {
                                mMessages.add(message);
                            }
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMessagesAdapter.notifyDataSetChanged();
                                mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);
                            }
                        });
                    }
                });

        ArrayList<Message> filteredMessages = new ArrayList<>();
        if (searchQuery.equals("")) {
            filteredMessages = unfilteredMessages;
        } else {
            for (Message message : unfilteredMessages){
                if (message.mTags.contains(searchQuery)){
                    filteredMessages.add(message);
                }
            }
        }

        mMessages.clear();
        mMessages.addAll(filteredMessages);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessagesAdapter.notifyDataSetChanged();
                mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);
            }
        });
    }

    public void onPause() {
        try {
            getActivity().unregisterReceiver(messageReceiver);
        } catch (IllegalArgumentException e) {
            super.onPause();
        }
        super.onPause();
    }

    public class MessageReceiver extends BroadcastReceiver {
        public MessageReceiver() { }
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.mecolab.memeticameandroid.MESSAGE_RECEIVED_ACTION")) {
                addGcmMessage(intent);
                abortBroadcast();
            }
        }
    }

    private void addGcmMessage(Intent intent) {
        final Message message = Message.getMessage(getActivity(),
                intent.getIntExtra("SERVER_ID", -1));
        if (message.mChannelId == mChannel.mServerId) {
            mMessages.add(message);
            mMessagesAdapter.notifyDataSetChanged();
            mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);

        }
    }

    private void setViews() {
        mMessages = Message.getMessagesFromChannel(getActivity(), mChannel,
                new Listeners.OnGetMemesListener() {
                    @Override
                    public void onMemesReceived(final ArrayList<Message> messages) {
                        if (getActivity() == null) return;
                        Collections.sort(messages, new Comparator<Message>() {
                            @Override
                            public int compare(Message o1, Message o2) {
                                return o1.mServerId - o2.mServerId;
                            }
                        });
                        for (Message message : messages) {
                            if (message.save(getActivity())) {
                                mMessages.add(message);
                            }
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMessagesAdapter.notifyDataSetChanged();
                                mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);
                            }
                        });
                    }
                });
        mMessagesAdapter =
                new MessageAdapter(getActivity(), R.layout.message_text_list_item, mMessages, true);
        mMessagesView.setAdapter(mMessagesAdapter);
        mMessagesView.setSelection(mMessagesAdapter.getCount() - 1);

        mMessagesView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int pos, long id) {

                Log.v("long clicked", "Message: " + mMessages.get(pos).mContent);

                if (mActionMode != null) {
                    return false;
                }

                // Start the CAB using the ActionMode.Callback defined above
                mActionMode = getActivity().startActionMode(mActionModeCallback);
                mActionMode.setTag(pos);
                view.setSelected(true);

                return true;
            }
        });
    }

    private void setChannel() {
        int channelServerId = getActivity().getIntent().getIntExtra(Channel.SERVER_ID, 0);
        mChannel = Channel.getChannel(getActivity(), channelServerId);
    }

    public void sendFileMessage(Uri uri, String mimeType) {
        showTagPrompt(uri, mimeType);
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_copy, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_copy:
                    Message msg = mMessages.get((int) mActionMode.getTag());
                    ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);

                    //ClipData clip = ClipData.newPlainText("URI", msg.mContent);
                    ClipData clip = ClipData.newUri(getContext().getContentResolver(), "URI", Uri.parse(msg.mContent));
                    clipboard.setPrimaryClip(clip);


                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_paste, menu);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle("Paste?");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_paste:
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);

                if (clipboard.hasPrimaryClip()) {

                    Uri pasteUri = clipboard.getPrimaryClip().getItemAt(0).getUri();
                    //String mimeType = FileManager.getMimeType(pasteData);

                    String mimeType = FileManager.getMimeType(pasteUri.toString());
                    if(mimeType != null) {
                        String tags = "";
                        sendFileMessage(pasteUri, mimeType);
                    }
                }
                break;
        }
        return true;
    }

    public void showTagPrompt (final Uri uri, final String mimeType){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add comma-separated tags to your meme");

        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Calendar c = Calendar.getInstance();
                Date date = c.getTime();
                String stringDate = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);
                Message.Builder builder = new Message.Builder();
                Message message = builder
                        .setContent(uri.toString())
                        .setChannelId(mChannel.mServerId)
                        .setDate(stringDate)
                        .setMimeType(mimeType)
                        .setSender(User.getLoggedUser(getActivity()).mPhoneNumber)
                        .setTags(input.getText().toString())
                        .build();

                mMessages.add(message);
                mMessagesAdapter.notifyDataSetChanged();

                message.sendMemeToChannel(getActivity(), new Listeners.SendMessageListener() {
                    @Override
                    public void onMessageSent(Message msg) {

                    }
                });
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
