package com.mecolab.memeticameandroid.Persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mecolab.memeticameandroid.Models.Channel;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;

import java.util.ArrayList;



public class DatabaseManager {

    private static DataSource dataSource;
    private static DatabaseManager sInstance;
    private static Context mContext;

    private DatabaseManager(Context context){
        dataSource = new DataSource(context);
        mContext = context;
    }

    public static synchronized DatabaseManager getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new DatabaseManager(context.getApplicationContext());
        }
        else if (mContext == null)
            mContext = context;
        return sInstance;
    }

    public void insertContact(User contact){
        ContentValues values = new ContentValues();
        User existing = getContact(contact.mPhoneNumber);
        if(existing != null)
            return;
        values.put(DataSource.ColumnUser.USER_COLUMN_PHONE_NUMBER, contact.mPhoneNumber);
        values.put(DataSource.ColumnUser.USER_COLUMN_NAME, contact.mName);
        values.put(DataSource.ColumnUser.USER_COLUMN_SERVER_ID, contact.mServerId);
        dataSource.database.insert(DataSource.USER_TABLE_NAME, null, values);
    }

    public ArrayList<User> getContacts(){
        Cursor c = dataSource.database.query(
                DataSource.USER_TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
        User me = User.getLoggedUser(mContext);
        ArrayList<User> users = new ArrayList<>();
        if(c.getCount() == 0){
            c.close();
            return users;
        }
        c.moveToFirst();
        do{
            String phoneNumber = c.getString(c.getColumnIndexOrThrow(DataSource.ColumnUser.USER_COLUMN_PHONE_NUMBER));
            if(me == null){
                Log.e("DB-Manager.getContacts", "detLoggedUser returned null.");
                return null;
            }
            if (!me.mPhoneNumber.equals(phoneNumber))
                users.add(new User(
                        0,
                        c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnUser.USER_COLUMN_SERVER_ID)),
                        phoneNumber,
                        c.getString(c.getColumnIndexOrThrow(DataSource.ColumnUser.USER_COLUMN_NAME))
                ));
        } while(c.moveToNext());
        c.close();
        return users;
    }

    public User getContact(String phone){
        Cursor c = dataSource.database.query(
                DataSource.USER_TABLE_NAME,
                null,
                DataSource.ColumnUser.USER_COLUMN_PHONE_NUMBER + "=?",
                new String[] { phone },
                null,
                null,
                null
        );
        c.moveToFirst();
        if (c.getCount() == 0){
            c.close();
            return null;
        }
        User user = new User(
                c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnUser.ID_USER)),
                c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnUser.USER_COLUMN_SERVER_ID)),
                c.getString(c.getColumnIndexOrThrow(DataSource.ColumnUser.USER_COLUMN_PHONE_NUMBER)),
                c.getString(c.getColumnIndexOrThrow(DataSource.ColumnUser.USER_COLUMN_NAME)));
        c.close();
        return user;
    }

    public ArrayList<Message> getMessages(int conversation_id){
        Cursor c = dataSource.database.query(
                DataSource.MESSAGE_TABLE_NAME,
                null,
                DataSource.ColumnMessage.MESSAGE_COLUMN_CONVERSATION_ID + "=?",
                new String[] { Integer.toString(conversation_id) },
                null,
                null,
                DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID + " ASC");

        c.moveToFirst();
        ArrayList<Message> messages = new ArrayList<>();
        if(c.getCount() != 0)
            do{
                Message.Builder builder = new Message.Builder();
                builder.setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_ID)));
                builder.setServerId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID)));
                builder.setDate(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_DATE)));
                builder.setMimeType(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_MIME_TYPE)));
                builder.setContent(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_CONTENT)));
                builder.setSender(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_SENDER)));
                builder.setConversationId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_CONVERSATION_ID)));
                Message msg = builder.build();
                messages.add(msg);
            } while(c.moveToNext());
        c.close();
        return messages;
    }

    public ArrayList<Message> getMessagesForChannel(int channel_id){
        Cursor c = dataSource.database.query(
                DataSource.MESSAGE_TABLE_NAME,
                null,
                DataSource.ColumnMessage.MESSAGE_COLUMN_CHANNEL_ID + "=?",
                new String[] { Integer.toString(channel_id) },
                null,
                null,
                DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID + " ASC");

        c.moveToFirst();
        ArrayList<Message> messages = new ArrayList<>();
        if(c.getCount() != 0)
            do{
                Message.Builder builder = new Message.Builder();
                builder.setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_ID)));
                builder.setServerId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID)));
                builder.setDate(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_DATE)));
                builder.setMimeType(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_MIME_TYPE)));
                builder.setContent(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_CONTENT)));
                builder.setSender(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_SENDER)));
                builder.setChannelId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_CHANNEL_ID)));
                builder.setRating(c.getDouble(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_RATING)));
                builder.setTags(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_TAGS)));
                Message msg = builder.build();
                messages.add(msg);
            } while(c.moveToNext());
        c.close();
        return messages;
    }

    public Message getMessage(int serverId){
        Cursor c = dataSource.database.query(
                DataSource.MESSAGE_TABLE_NAME,
                null,
                DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID + "=?",
                new String[] { Integer.toString(serverId) },
                null,
                null,
                DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID + " ASC"
        );
        c.moveToFirst();
        if(c.getCount() == 0) {
            c.close();
            return null;
        }
        Message msg = new Message.Builder()
                .setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_ID)))
                .setServerId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID)))
                .setDate(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_DATE)))
                .setMimeType(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_MIME_TYPE)))
                .setContent(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_CONTENT)))
                .setSender(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_SENDER)))
                .setConversationId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_CONVERSATION_ID)))
                .setChannelId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_CHANNEL_ID)))
                .setRating(c.getDouble(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_RATING)))
                .setTags(c.getString(c.getColumnIndexOrThrow(DataSource.ColumnMessage.MESSAGE_COLUMN_TAGS)))
                .build();
        c.close();
        return msg;
    }

    public long updateMessage(Message msg){
        ContentValues values = new ContentValues();
        if (getMessage(msg.mServerId) == null)
            return -1;

        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID, msg.mServerId);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_MIME_TYPE, msg.mMimeType);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_SENDER, msg.mSender);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_CONTENT, msg.mContent);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_CONVERSATION_ID, msg.mConversationId);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_DATE, msg.mDate);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_RATING, msg.mRating);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_TAGS, msg.mTags);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_CHANNEL_ID, msg.mChannelId);

        return dataSource.database.update(
                DataSource.MESSAGE_TABLE_NAME,
                values,
                DataSource.ColumnMessage.MESSAGE_COLUMN_ID + "=?",
                new  String[] { Integer.toString(msg.mId)}
        );
    }

    public long insertMessage(Message msg){
        ContentValues values = new ContentValues();
        if (getMessage(msg.mServerId) != null)
            return -1;
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_SERVER_ID, msg.mServerId);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_MIME_TYPE, msg.mMimeType);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_SENDER, msg.mSender);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_CONTENT, msg.mContent);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_CONVERSATION_ID, msg.mConversationId);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_DATE, msg.mDate);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_RATING, msg.mRating);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_TAGS, msg.mTags);
        values.put(DataSource.ColumnMessage.MESSAGE_COLUMN_CHANNEL_ID, msg.mChannelId);
        return dataSource.database.insert(DataSource.MESSAGE_TABLE_NAME, null, values);
    }

    public Conversation getConversation(int serverId){
        Cursor c = dataSource.database.query(
                DataSource.CONVERSATION_TABLE_NAME,
                null,
                DataSource.ColumnConversation.CONVERSATION_COLUMN_SERVER_ID + "=?",
                new String[]{Integer.toString(serverId)},
                null,
                null,
                null);
        c.moveToFirst();
        if (c.getCount() == 0){
            c.close();
            return null;
        }
        boolean group = c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_GROUP)) == 1;
        ArrayList<User> participants = getConversationParticipants(serverId);

        Conversation conversation = new Conversation(
                c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_ID)),
                serverId,
                c.getString(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_TITLE)),
                c.getString(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_ADMIN_PHONE)),
                c.getString(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_CREATED_AT)),
                group,
                participants);
        c.close();
        return conversation;
    }

    public ArrayList<Conversation> getConversations(/*String userPhone*/){
         ArrayList<Conversation> conversations = new ArrayList<>();
         Cursor c = dataSource.database.query(
                 DataSource.CONVERSATION_TABLE_NAME,
                 null,
                 null, // DataSource.ColumnConversation.CONVERSATION_COLUMN_ADMIN_PHONE + "=?",
                 null, // new String[]{userPhone},
                 null,
                 null,
                 null
         );
        c.moveToFirst();
        if(c.getCount() == 0){
            c.close();
            return conversations;
        }

        do{
            boolean group = c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_GROUP)) == 1;
            int serverId = c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_SERVER_ID));
            Conversation conversation = new Conversation(
                    c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_ID)),
                    serverId,
                    c.getString(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_TITLE)),
                    c.getString(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_ADMIN_PHONE)),
                    c.getString(c.getColumnIndexOrThrow(DataSource.ColumnConversation.CONVERSATION_COLUMN_CREATED_AT)),
                    group,
                    getConversationParticipants(serverId));
            conversations.add(conversation);
        } while(c.moveToNext());
        c.close();
        return conversations;
    }

    public void insertConversationParticipant(int ConversationServerId, String userPhone){
        ContentValues values = new ContentValues();
        values.put(DataSource.ColumnUserConversations.USER_CONVERSATION_CONVERSATION_ID, ConversationServerId);
        values.put(DataSource.ColumnUserConversations.USER_CONVERSATION_USER_PHONE, userPhone);
        dataSource.database.insert(DataSource.USER_CONVERSATION_TABLE_NAME, null, values);
    }

    public ArrayList<User> getConversationParticipants(int ConversationServerId){
        ArrayList<User> participants = new ArrayList<>();
        Cursor c = dataSource.database.query(
                 DataSource.USER_CONVERSATION_TABLE_NAME,
                 null,
                 DataSource.ColumnUserConversations.USER_CONVERSATION_CONVERSATION_ID + "=?",
                 new String[]{Integer.toString(ConversationServerId)},
                 null,
                 null,
                 null
         );
        c.moveToFirst();
        if(c.getCount() == 0){
            c.close();
            return participants;
        }

        do{
            User user = User.getUserOrCreate(mContext,
                            c.getString(c.getColumnIndexOrThrow(DataSource.ColumnUserConversations
                            .USER_CONVERSATION_USER_PHONE)));
            participants.add(user);

        } while(c.moveToNext());
        c.close();
        return participants;
    }

    public boolean insertConversation(Conversation conversation){
        Conversation existing = Conversation.getConversation(mContext, conversation.mServerId);
        if(existing != null)
            return false;
        ContentValues values = new ContentValues();
        values.put(DataSource.ColumnConversation.CONVERSATION_COLUMN_SERVER_ID, conversation.mServerId);
        values.put(DataSource.ColumnConversation.CONVERSATION_COLUMN_TITLE, conversation.mTitle);
        values.put(DataSource.ColumnConversation.CONVERSATION_COLUMN_ADMIN_PHONE, conversation.mAdminPhone);
        values.put(DataSource.ColumnConversation.CONVERSATION_COLUMN_CREATED_AT, conversation.mCreatedAt);
        values.put(DataSource.ColumnConversation.CONVERSATION_COLUMN_GROUP, conversation.mIsGroup);
        dataSource.database.insert(DataSource.CONVERSATION_TABLE_NAME, null, values);

        for(User user : conversation.mParticipants){
            insertConversationParticipant(conversation.mServerId, user.mPhoneNumber);
        }
        return true;
    }

    public ArrayList<Channel> getChannels(){
        ArrayList<Channel> channels = new ArrayList<>();
        Cursor c = dataSource.database.query(
                DataSource.CHANNEL_TABLE_NAME,
                null,
                null, // DataSource.ColumnConversation.CONVERSATION_COLUMN_ADMIN_PHONE + "=?",
                null, // new String[]{userPhone},
                null,
                null,
                null
        );
        c.moveToFirst();
        if(c.getCount() == 0){
            c.close();
            return channels;
        }

        do{
            int serverId = c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_SERVER_ID));
            String name = c.getString(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_NAME));
            String admin_phone = c.getString(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_ADMIN_PHONE));
            User admin = User.getUserOrCreate(mContext, admin_phone);
            String categories = c.getString(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_CATEGORIES));
            Channel channel = new Channel(
                    c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_ID)),
                    serverId,
                    name,
                    admin,
                    categories);
            channels.add(channel);
        } while(c.moveToNext());
        c.close();
        return channels;
    }

    public Channel getChannel(int serverId){
        Cursor c = dataSource.database.query(
                DataSource.CHANNEL_TABLE_NAME,
                null,
                DataSource.ColumnChannels.CHANNEL_COLUMN_SERVER_ID + "=?",
                new String[]{Integer.toString(serverId)},
                null,
                null,
                null);
        c.moveToFirst();
        if (c.getCount() == 0){
            c.close();
            return null;
        }

        Channel channel = new Channel(
                c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_ID)),
                serverId,
                c.getString(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_NAME)),
                User.getUserOrCreate(mContext,
                        c.getString(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_ADMIN_PHONE))),
                c.getString(c.getColumnIndexOrThrow(DataSource.ColumnChannels.CHANNEL_COLUMN_CATEGORIES)));
        c.close();
        return channel;
    }

    public boolean insertChannel(Channel channel){
        Channel existing = Channel.getChannel(mContext, channel.mServerId);
        if(existing != null)
            return false;
        ContentValues values = new ContentValues();
        values.put(DataSource.ColumnChannels.CHANNEL_COLUMN_SERVER_ID, channel.mServerId);
        values.put(DataSource.ColumnChannels.CHANNEL_COLUMN_NAME, channel.mName);
        values.put(DataSource.ColumnChannels.CHANNEL_COLUMN_ADMIN_PHONE, channel.mAdmin.mPhoneNumber);
        values.put(DataSource.ColumnChannels.CHANNEL_COLUMN_CATEGORIES, channel.mCategories);
        dataSource.database.insert(DataSource.CHANNEL_TABLE_NAME, null, values);
        
        return true;
    }

    public ArrayList<Invitation> getInvitations(){
        Cursor c = dataSource.database.query(
                DataSource.INVITATION_TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);

        c.moveToFirst();
        ArrayList<Invitation> invitations = new ArrayList<>();
        if(c.getCount() != 0)
            do{
                Invitation invitation = new Invitation.Builder()
                        .setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_ID)))
                        .setConversation(Conversation.getConversation(mContext, c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID))))
                        .setStatus(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS)))
                        .setUser(User.getUser(mContext, c.getString(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE))))
                        .build();
                invitations.add(invitation);
            } while(c.moveToNext());
        c.close();
        return invitations;
    }

    public ArrayList<Invitation> getInvitations(int status){
        Cursor c = dataSource.database.query(
                DataSource.INVITATION_TABLE_NAME,
                null,
                DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS + "=?",
                new String[] {Integer.toString(Invitation.INVITATION_PENDING) },
                null,
                null,
                null);

        c.moveToFirst();
        ArrayList<Invitation> invitations = new ArrayList<Invitation>();
        if(c.getCount() != 0)
            do{
                Invitation invitation = new Invitation.Builder()
                        .setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_ID)))
                        .setConversation(Conversation.getConversation(mContext, c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID))))
                        .setStatus(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS)))
                        .setUser(User.getUser(mContext, c.getString(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE))))
                        .build();
                invitations.add(invitation);
            } while(c.moveToNext());
        c.close();
        return invitations;
    }

    public Invitation getInvitation(User user, int conversationId){
        Cursor c = dataSource.database.query(
                DataSource.INVITATION_TABLE_NAME,
                null,
                DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE + "=? AND " + DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID + "=?",
                new String[] { user.mPhoneNumber, Integer.toString(conversationId) },
                null,
                null,
                DataSource.ColumnInvitations.INVITATION_COLUMN_ID + " ASC"
        );
        c.moveToFirst();
        if(c.getCount() == 0) {
            c.close();
            return null;
        }
        Invitation invitation = new Invitation.Builder()
                .setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_ID)))
                .setConversation(Conversation.getConversation(mContext, c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID))))
                .setStatus(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS)))
                .setUser(User.getUser(mContext, c.getString(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE))))
                .build();
        c.close();
        return invitation;
    }

    public Invitation getInvitation(String user_phone, int conversationId){
        Cursor c = dataSource.database.query(
                DataSource.INVITATION_TABLE_NAME,
                null,
                DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE + "=? AND " + DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID + "=?",
                new String[] { user_phone, Integer.toString(conversationId) },
                null,
                null,
                DataSource.ColumnInvitations.INVITATION_COLUMN_ID + " ASC"
        );
        c.moveToFirst();
        if(c.getCount() == 0) {
            c.close();
            return null;
        }
        Invitation invitation = new Invitation.Builder()
                .setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_ID)))
                .setConversation(Conversation.getConversation(mContext, c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID))))
                .setStatus(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS)))
                .setUser(User.getUser(mContext, c.getString(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE))))
                .build();
        c.close();
        return invitation;
    }

    public Invitation getInvitation(int invitationId){
        Cursor c = dataSource.database.query(
                DataSource.INVITATION_TABLE_NAME,
                null,
                DataSource.ColumnInvitations.INVITATION_COLUMN_ID + "=?",
                new String[] { Integer.toString(invitationId) },
                null,
                null,
                DataSource.ColumnInvitations.INVITATION_COLUMN_ID + " ASC"
        );
        c.moveToFirst();
        if(c.getCount() == 0) {
            c.close();
            return null;
        }
        Invitation invitation = new Invitation.Builder()
                .setId(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_ID)))
                .setConversation(Conversation.getConversation(mContext, c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID))))
                .setStatus(c.getInt(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS)))
                .setUser(User.getUser(mContext, c.getString(c.getColumnIndexOrThrow(DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE))))
                .build();
        c.close();
        return invitation;
    }

    public long insertInvitation(Invitation invitation){
        ContentValues values = new ContentValues();
        if (getInvitation(
                invitation.mUser, invitation.mConversation.mServerId
        ) != null)
            return -1;

        values.put(DataSource.ColumnInvitations.INVITATION_COLUMN_CONVERSATION_ID, invitation.mConversation.mServerId);
        values.put(DataSource.ColumnInvitations.INVITATION_COLUMN_USER_PHONE, invitation.mUser.mPhoneNumber);
        values.put(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS, invitation.mStatus);

        long result = dataSource.database.insert(DataSource.INVITATION_TABLE_NAME, null, values);
        return result;
    }

    public long acceptInvitation(Invitation invitation){
        ContentValues values = new ContentValues();
        if (getInvitation(
                invitation.mUser, invitation.mConversation.mServerId
        ) == null)
            return -1;
        values.put(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS, 1);

        return dataSource.database.update(
                DataSource.INVITATION_TABLE_NAME,
                values,
                DataSource.ColumnInvitations.INVITATION_COLUMN_ID + "=?",
                new  String[] { Integer.toString(invitation.mId)}
        );
    }

    public long declineInvitation(Invitation invitation){
        ContentValues values = new ContentValues();
        if (getInvitation(
                invitation.mUser, invitation.mConversation.mServerId
        ) == null)
            return -1;
        values.put(DataSource.ColumnInvitations.INVITATION_COLUMN_STATUS, -1);

        return dataSource.database.update(
                DataSource.INVITATION_TABLE_NAME,
                values,
                DataSource.ColumnInvitations.INVITATION_COLUMN_ID + "=?",
                new  String[] { Integer.toString(invitation.mId)}
        );
    }
}
