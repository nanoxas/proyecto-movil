package com.mecolab.memeticameandroid.Networking;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mecolab.memeticameandroid.Models.Channel;
import com.mecolab.memeticameandroid.Utils.FileManager;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class NetworkingManager {

    public static final String BASE_URL = "http://mcctrack4.ing.puc.cl/api/v2/";
    private static final int DEFAULT_TIMEOUT = 10000; //10 seconds
    private static NetworkingManager mInstance;
    private static Context mContext;
    private RequestQueue mRequestQueue;
    private RetryPolicy mRetryPolicy;
    private String mToken;

    private NetworkingManager(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
        mRetryPolicy = new DefaultRetryPolicy(DEFAULT_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    public static synchronized NetworkingManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkingManager(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    private String getToken() {
        if (mToken != null){
            return mToken;
        }
        else {
            SharedPreferences sharedPref = mContext.getSharedPreferences(
                    mContext.getString(R.string.SharedPreferences_Preferences), Context.MODE_PRIVATE);
            mToken = sharedPref.getString(
                    mContext.getString(R.string.SharedPreferences_Token), "0");
            return mToken;
        }
    }

    private <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(mRetryPolicy);
        getRequestQueue().add(req);
    }

    public void createUser(String phone, String name, String password, final Listeners.OnAuthListener listener) {

        String url = BASE_URL + "users";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone_number", phone);
            jsonObject.put("name", name);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            User user = new User(
                                    0,
                                    response.getInt("id"),
                                    response.getString("phone_number"),
                                    response.getString("name"));
                            String token = response.getString("api_key");

                            SharedPreferences sharedPref = mContext.getSharedPreferences(
                                    mContext.getString(R.string.SharedPreferences_Preferences),
                                    Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(mContext.getString(R.string.SharedPreferences_PhoneNumber),
                                    user.mPhoneNumber);
                            editor.putString(mContext.getString(R.string.SharedPreferences_Token),
                                    token);
                            editor.apply();

                            listener.onUserReceived(user);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onUserReceived(null);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        addToRequestQueue(jsObjRequest);
    }

    public void getUsers(final Listeners.ServerGetContactsListener listener) {
        String url = BASE_URL + "users";
        JSONObject jsonObject = new JSONObject();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                jsonObject,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            int len = response.length();
                            ArrayList<User> users = new ArrayList<>();
                            for (int i = 0; i < len; i++) {
                                JSONObject json = response.getJSONObject(i);
                                User user = new User(
                                        0,
                                        json.getInt("id"),
                                        json.getString("phone_number"),
                                        json.getString("name"));
                                users.add(user);
                            }
                            listener.onContactsReceived(users);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsonArrayRequest);
    }

    public void getMessages(int conversationServerId, int lastMessageId,
                            final ArrayList<Message> savedMessages,
                            final Listeners.OnGetMessagesListener listener) {

        String url = BASE_URL +
                "conversations/get_messages?conversation_id=" + conversationServerId +
                "&last_message_id=" + lastMessageId;
        JSONObject jsonObject = new JSONObject();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                jsonObject,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray resp) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    ArrayList<Message> messages = new ArrayList<>();
                                    int size = resp.length();
                                    for (int i = 0; i < size; i++) {
                                        JSONObject response = resp.getJSONObject(i);
                                        String mimeType;
                                        Message.Builder builder = new Message.Builder()
                                                .setServerId(response.getInt("id"));
                                        if (response.isNull("file")) {
                                            mimeType = "plain/text";
                                            builder.setContent(response.getString("content"));
                                        } else {
                                            JSONObject file = response.getJSONObject("file");
                                            mimeType = file.getString("mime_type");
                                            builder.setContent(downloadFile(file.getString("url"),
                                                    FileManager.BASE_PATH + "/" +
                                                            FileManager.generateFileName(mimeType)).toString())
                                                    .setUrl(file.getString("url"));

                                        }

                                        Message msg = builder.setSender(response.getString("sender"))
                                                .setMimeType(mimeType)
                                                .setConversationId(response.getInt("conversation_id"))
                                                .build();
                                        boolean messageAlreadyExists = false;

                                        for (Message savedMessage : savedMessages) {
                                            if (msg.mServerId == savedMessage.mServerId) {
                                                messageAlreadyExists = true;
                                                break;
                                            }
                                        }
                                        if (messageAlreadyExists) {
                                            continue;
                                        }
                                        messages.add(msg);
                                    }
                                    listener.onMessagesReceived(messages);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsonArrayRequest);
    }

    public void sendMessage(Message msg, final Listeners.SendMessageListener listener) {

        String url = BASE_URL + "conversations/send_message";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sender", msg.mSender);
            jsonObject.put("conversation_id", msg.mConversationId);
            if (msg.mType.equals(Message.MessageType.AUDIO)) {
                Uri uri = Uri.parse(msg.mContent);
                try {
                    JSONObject fileObject = new JSONObject();
                    fileObject.put("file_name", "record.3gp");
                    fileObject.put("content", msg.mContent);
                    fileObject.put("mime_type", msg.mMimeType);
                    jsonObject.put("file", fileObject);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (!msg.mType.equals(Message.MessageType.TEXT)) {
                Uri uri = Uri.parse(msg.mContent);
                try {
                    JSONObject fileObject = new JSONObject();
                    String filename=uri.getLastPathSegment();
                    if (uri.getLastPathSegment().split("/").length > 1 && msg.mMimeType.contains("image")) {
                         filename = uri.getLastPathSegment().split("/")[1];
                    }


                    fileObject.put("file_name", filename);
                    fileObject.put("content", FileManager.loadBase64(mContext, uri));
                    fileObject.put("mime_type", msg.mMimeType);
                    jsonObject.put("file", fileObject);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                jsonObject.put("content", msg.mContent);
            }

            JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                    Request.Method.POST,
                    url,
                    jsonObject,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(final JSONArray resp) {
                            Thread thread = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        JSONObject response = resp.getJSONObject(0);
                                        String mimeType;
                                        Message.Builder builder = new Message.Builder()
                                                .setServerId(response.getInt("id"));
                                        if (response.isNull("file")) {
                                            mimeType = "plain/text";
                                            builder.setContent(response.getString("content"));
                                        } else {
                                            JSONObject file = response.getJSONObject("file");
                                            mimeType = file.getString("mime_type");
                                            builder.setContent(downloadFile(file.getString("url"),
                                                    FileManager.BASE_PATH + "/" +
                                                            FileManager.generateFileName(mimeType)).toString());

                                        }

                                        Message msg = builder.setSender(response.getString("sender"))
                                                .setMimeType(mimeType)
                                                .setConversationId(response.getInt("conversation_id"))
                                                .build();
                                        listener.onMessageSent(msg);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Log.e("NETWORK ERROR", "On Response Error");
                                        Toast.makeText(mContext, "Network Connection Error: File not sent", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            };
                            thread.start();

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("NETWORK ERROR", error.toString());
                            Toast.makeText(mContext, "Network Connection Error: File not sent", Toast.LENGTH_SHORT).show();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Token token=" + getToken());
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getChannels(final ArrayList<Channel> savedChannels,
                                 final Listeners.OnGetChannelsListener listener) {
        String url = BASE_URL + "channels";
        JSONObject jsonObject = new JSONObject();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                jsonObject,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            ArrayList<Channel> channels = new ArrayList<>();
                            int size = response.length();
                            for (int i = 0; i < size; i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                String name = jsonObject.getString("name");
                                int id = jsonObject.getInt("id");
                                if (jsonObject.isNull("name") || jsonObject.isNull("admin"))
                                    continue;
                                String admin_name = jsonObject.getJSONObject("admin").getString("name");
                                String admin_phone_number = jsonObject.getJSONObject("admin").getString("phone_number");
                                User admin = User.getUserOrCreate(mContext, admin_phone_number, admin_name);
                                String categories = jsonObject.getString("category");
                                boolean channelAlreadyExists = false;
                                for (Channel savedChannel : savedChannels) {
                                    if (savedChannel.mServerId == id){
                                        channelAlreadyExists = true;
                                        break;
                                    }
                                }

                                if (channelAlreadyExists)
                                    continue;

                                Channel channel = new Channel(
                                        0,
                                        id,
                                        name,
                                        admin,
                                        categories
                                );
                                channels.add(channel);
                                channel.save(mContext);
                            }
                            listener.onChannelsReceived(channels);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("API error","channels");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsonArrayRequest);
    }
    public void createChannel(User admin, String name, String categories, final Listeners.OnGetNewChannelListener listener) {
        String url = BASE_URL + "channels";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("admin_phone", admin.mPhoneNumber);
            jsonObject.put("category", categories);
            jsonObject.put("name", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = response;
                            String name = jsonObject.getString("name");
                            int id = jsonObject.getInt("id");
                            String admin_name = jsonObject.getJSONObject("admin").getString("name");
                            String admin_phone_number = jsonObject.getJSONObject("admin").getString("phone_number");
                            User admin = User.getUserOrCreate(mContext, admin_phone_number, admin_name);
                            String categories = jsonObject.getString("category");

                            Channel channel = new Channel(
                                    0,
                                    id,
                                    name,
                                    admin,
                                    categories
                            );
                            channel.save(mContext);
                            listener.onChannelReceived(channel);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onChannelReceived(null);

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsObjRequest);
    }

    public void getMemesFromChannel(final Channel channel, final ArrayList<Message> savedMemes,
                                    final Listeners.OnGetMemesListener listener)
    {
        String url = BASE_URL + "memes?channel_id=" + channel.mServerId;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                new JSONObject(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray response) {

                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                ArrayList<Message> memes = new ArrayList<>();
                                try {
                                    int size = response.length();
                                    for (int i = 0; i < size; i++) {
                                        Message.Builder messageBuilder = new Message.Builder();
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        JSONArray tagsJSON = jsonObject.getJSONArray("tags");
                                        String tags = "";
                                        for (int j = 0; j < tagsJSON.length(); j++) {
                                            tags += tagsJSON.getString(j) + ",";
                                        }
                                        tags.substring(0, tags.length() - 1); // remove last comma
                                        int id = jsonObject.getInt("id");
                                        String mimeType = jsonObject.getString("mimetype");
                                        messageBuilder.setContent(downloadFile(jsonObject.getString("link"),
                                                FileManager.BASE_PATH + "/" +
                                                        FileManager.generateFileName(mimeType)).toString());

                                        boolean messageAlreadyExists = false;

                                        Message message = messageBuilder.setSender("")
                                                .setMimeType(mimeType)
                                                .setServerId(id)
                                                .setChannelId(channel.mServerId)
                                                .setUrl(jsonObject.getString("link"))
                                                .setServerId(jsonObject.getInt("id"))
                                                .setRating(jsonObject.getDouble("rating"))
                                                .setTags(tags)
                                                .build();
                                        for (Message savedMessage : savedMemes) {
                                            if (message.mServerId == savedMessage.mServerId) {
                                                messageAlreadyExists = true;
                                                break;
                                            }
                                        }
                                        if (messageAlreadyExists) {
                                            continue;
                                        }

                                        message.save(mContext);
                                        memes.add(message);
                                    }
                                    listener.onMemesReceived(memes);
                                } catch (
                                        JSONException e
                                        )

                                {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Channel", "failed to load memes");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsonArrayRequest);

    }

    public void sendMemeToChannel(final Message msg, final Listeners.SendMessageListener listener) {

        String url = BASE_URL + "memes";
        JSONObject jsonObject = new JSONObject();
        try {
            String[] tagsArray = msg.mTags.split(",");
            jsonObject.put("tags", new JSONArray(Arrays.asList(tagsArray)));
            jsonObject.put("channel_id", msg.mChannelId);
            if (msg.mType.equals(Message.MessageType.MMA) || msg.mType.equals(Message.MessageType.IMAGE)) {
                Uri uri = Uri.parse(msg.mContent);
                String filename=uri.getLastPathSegment();
                if (uri.getLastPathSegment().split("/").length > 1 && msg.mMimeType.contains("image")) {
                    filename = uri.getLastPathSegment().split("/")[1];
                }
                try {
                    jsonObject.put("file_name", filename);
                    jsonObject.put("content", FileManager.loadBase64(mContext, uri));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                return;
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.POST,
                    url,
                    jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(final JSONObject response) {
                            Log.d("Meme", "success");
                            Thread thread = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        String mimeType;
                                        Message.Builder builder = new Message.Builder()
                                                .setServerId(response.getInt("id"));

                                        mimeType = response.getString("mimetype");
                                        builder.setContent(downloadFile(response.getString("link"),
                                                FileManager.BASE_PATH + "/" +
                                                        FileManager.generateFileName(mimeType)).toString());
                                        JSONArray tagsJSON = response.getJSONArray("tags");
                                        String tags = "";
                                        for (int j=0; j < tagsJSON.length(); j++){
                                            tags += tagsJSON.getString(j) + ",";
                                        }
                                        tags.substring(0,tags.length() - 1); // remove last comma

                                        Message message = builder
                                                .setMimeType(mimeType)
                                                .setChannelId(msg.mChannelId)
                                                .setRating(response.getDouble("rating"))
                                                .setTags(tags)
                                                .build();
                                        message.save(mContext);
                                        listener.onMessageSent(message);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            thread.start();

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Meme", "sending failed");
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                Log.d("Response code", response.statusCode + "");
                                Log.d("Response body", new String(response.data));
                            }
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Token token=" + getToken());
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void rateMeme(User user, final Message meme, double rating, final Listeners.OnRateMemeListener listener){
        String url = BASE_URL + "memes/rate";

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("meme_id", meme.mServerId);
            jsonObject.put("phone_number", user.mPhoneNumber);
            jsonObject.put("rating", rating);
        } catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    String mimeType;
                                    int id = response.getInt("id");
                                    Message message = Message.getMessage(mContext, id);
                                    if (message != null){
                                        double rating = response.getDouble("rating");
                                        message.setRating(rating);
                                        message.update(mContext);
                                    } else {
                                        Message.Builder builder = new Message.Builder()
                                                .setServerId(response.getInt("id"));

                                        mimeType = response.getString("mimetype");
                                        builder.setContent(downloadFile(response.getString("link"),
                                                FileManager.BASE_PATH + "/" +
                                                        FileManager.generateFileName(mimeType)).toString());
                                        JSONArray tagsJSON = response.getJSONArray("tags");
                                        String tags = "";
                                        for (int j=0; j < tagsJSON.length(); j++){
                                            tags += tagsJSON.getString(j) + ",";
                                        }
                                        tags.substring(0,tags.length() - 1); // remove last comma

                                        message = builder.setSender(response.getString("sender"))
                                                .setMimeType(mimeType)
                                                .setChannelId(meme.mChannelId)
                                                .setRating(response.getDouble("rating"))
                                                .setTags(tags)
                                                .build();
                                        message.save(mContext);
                                    }

                                    listener.onMemeRated(message);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Meme", "rating failed");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsonObjectRequest);

    }

    public void getConversations(String userPhone, final ArrayList<Conversation> savedConversations,
                                 final Listeners.OnGetConversationsListener listener) {
        String url = BASE_URL + "users/get_conversations?phone_number=" + userPhone;
        JSONObject jsonObject = new JSONObject();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                jsonObject,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response", response.toString());
                            ArrayList<Conversation> conversations = new ArrayList<>();
                            int size = response.length();
                            for (int i = 0; i < size; i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                String title = jsonObject.getString("title");
                                if (!jsonObject.getBoolean("group")) {
                                    User me = User.getLoggedUser(mContext);
                                    User user1 = User.getUserOrCreate(mContext,
                                            ((JSONObject) jsonObject.getJSONArray("users")
                                                    .get(0)).getString("phone_number"));
                                    User user2 = User.getUserOrCreate(mContext,
                                            ((JSONObject) jsonObject.getJSONArray("users")
                                                    .get(1)).getString("phone_number"));
                                    if (!user1.equals(me)) {
                                        title = user1.mName;
                                    } else {
                                        title = user2.mName;
                                    }
                                }
                                int id = jsonObject.getInt("id");

                                boolean conversationAlreadyExists = false;
                                //boolean conversationNeedsUpdate = false;
                                JSONArray jsonParticipants = jsonObject.getJSONArray("users");
                                ArrayList<User> participants = new ArrayList<>();
                                int len = jsonParticipants.length();
                                for (int j = 0; j < len; j++) {
                                    User me = User.getLoggedUser(mContext);
                                    if (me.mPhoneNumber.equals(jsonParticipants.getJSONObject(j).getString("phone_number")) &&
                                            jsonObject.getBoolean("group") &&
                                            !jsonParticipants.getJSONObject(j).getBoolean("accepted")) {
                                        Invitation invite = new Invitation.Builder()
                                                .setConversation(Conversation.getConversation(mContext, id))
                                                .setId(0)
                                                .setStatus(Invitation.INVITATION_PENDING)
                                                .setUser(me)
                                                .build();
                                        invite.save(mContext);
                                    }


                                    User u = User.getUserOrCreate(mContext,
                                            jsonParticipants.getJSONObject(j).getString("phone_number"),
                                            jsonParticipants.getJSONObject(j).getString("name"));
                                    participants.add(u);
                                }

                                for (Conversation savedConversation : savedConversations) {
                                    if (savedConversation.mServerId == id) {
                                        if (savedConversation.mParticipants.size() == participants.size()) {
                                            conversationAlreadyExists = true;
                                            break;
                                        } else {
                                            conversationAlreadyExists = true;
                                            break;
                                        }
                                    }
                                }

                                if (conversationAlreadyExists)
                                    continue;

                                Conversation conversation = new Conversation(
                                        0,
                                        id,
                                        title,
                                        jsonObject.getString("admin"),
                                        jsonObject.getString("created_at"),
                                        jsonObject.getBoolean("group"),
                                        participants);
                                conversations.add(conversation);
                            }
                            listener.onConversationsReceived(conversations);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsonArrayRequest);
    }

    public void newTwoConversation(User user1, User user2, final Listeners.OnGetNewTwoConversationListener listener) {
        String url = BASE_URL + "conversations";
        JSONObject jsonObject = new JSONObject();
        ArrayList<String> usersPhoneNumbers = new ArrayList<>();
        usersPhoneNumbers.add(user1.mPhoneNumber);
        usersPhoneNumbers.add(user2.mPhoneNumber);
        try {
            jsonObject.put("admin", user1.mPhoneNumber);
            jsonObject.put("group", false);
            jsonObject.put("users", new JSONArray(usersPhoneNumbers));
            jsonObject.put("title", user2.mName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonParticipants = response.getJSONArray("users");
                            ArrayList<User> participants = new ArrayList<>();
                            int len = jsonParticipants.length();
                            for (int i = 0; i < len; i++) {
                                User user = User.getUserOrCreate(mContext,
                                        jsonParticipants.getJSONObject(i).getString("phone_number"),
                                        jsonParticipants.getJSONObject(i).getString("name"));
                                participants.add(user);
                            }
                            Conversation conversation = new Conversation(
                                    0,
                                    response.getInt("id"),
                                    response.getString("title"),
                                    response.getString("admin"),
                                    response.getString("created_at"),
                                    false,
                                    participants);

                            listener.onConversationReceived(conversation);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onConversationReceived(null);

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsObjRequest);
    }

    public void newGroupConversation(ArrayList<User> users, String title,
                                     final Listeners.OnGetNewGroupConversationListener listener) {
        String url = BASE_URL + "conversations";
        JSONObject jsonObject = new JSONObject();
        try {
            ArrayList<String> jsonUsers = new ArrayList<>();
            for (User u : users) {
                jsonUsers.add(u.mPhoneNumber);
            }
            jsonObject.put("admin", users.get(0).mPhoneNumber);
            jsonObject.put("title", title);
            jsonObject.put("group", true);
            jsonObject.put("users", new JSONArray(jsonUsers));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonParticipants = response.getJSONArray("users");
                            ArrayList<User> participants = new ArrayList<>();
                            int len = jsonParticipants.length();
                            for (int i = 0; i < len; i++) {
                                User user = User.getUserOrCreate(mContext,
                                        jsonParticipants.getJSONObject(i).getString("phone_number"),
                                        jsonParticipants.getJSONObject(i).getString("name"));
                                participants.add(user);
                            }
                            Conversation conversation = new Conversation(
                                    0,
                                    response.getInt("id"),
                                    response.getString("title"),
                                    response.getString("admin"),
                                    response.getString("created_at"),
                                    true,
                                    participants);
                            listener.onConversationReceived(conversation);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onConversationReceived(null);

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsObjRequest);

    }

    public void acceptInvite(int conversation) {

        String url = BASE_URL + "conversations/accept_invitation";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone_number", User.getLoggedUser(mContext).mPhoneNumber);
            jsonObject.put("conversation_id", conversation);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsObjRequest);


    }


    public void addParticipantToConversation(int conversationId, String phoneNumber,
                                             final Listeners.OnUserAddedListener listener) {
        String url = BASE_URL + "conversations/add_user";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone_number", phoneNumber);
            jsonObject.put("conversation_id", conversationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onUserAdded(true);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onUserAdded(false);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        addToRequestQueue(jsObjRequest);
    }

    public void registerGCM(String token) {
        String url = BASE_URL + "users/gcm_register";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone_number", User.getLoggedUser(mContext).mPhoneNumber);
            jsonObject.put("registration_id", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token token=" + getToken());
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        addToRequestQueue(jsObjRequest);
    }

    public static Uri downloadFile(String urlToDownload, String path) {


        try {
            URL url = new URL(urlToDownload);
            URLConnection connection = url.openConnection();
            connection.connect();

            // download the file
            InputStream input = new BufferedInputStream(connection.getInputStream());

            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath() + "/path");
            boolean created = dir.mkdirs();


            OutputStream output = new FileOutputStream(path);

            int fileLength = connection.getContentLength();

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    Log.i("DownloadProgress", ""+((int) (total * 100 / fileLength)));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Network Connection Failed: file not downloaded", Toast.LENGTH_SHORT).show();
        }

        return Uri.fromFile(new File(path));
    }
}