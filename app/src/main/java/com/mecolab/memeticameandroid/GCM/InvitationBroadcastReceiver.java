package com.mecolab.memeticameandroid.GCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.mecolab.memeticameandroid.Activities.ConversationActivity;
import com.mecolab.memeticameandroid.Activities.MainActivity;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Mecolab on 01-11-2016.
 */

public class InvitationBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int conversationId = intent.getIntExtra("conversation_id",-1);
        String user_phone = intent.getStringExtra("user_phone");

        Conversation conversation = Conversation.getConversation(context, conversationId);
        Invitation invitation = Invitation.getInvitation(context, user_phone, conversationId);

        sendNotification(context, conversation, invitation);
    }

    private void sendNotification(Context context, Conversation conversation, Invitation invitation) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(conversation.mTitle)
                        .setContentText("Invitation received");
        // Sets an ID for the notification
        int mNotificationId = invitation.mId;
        Intent invitationsIntent = new Intent(context, MainActivity.class);
        invitationsIntent.putExtra("openTab", 2);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                invitationsIntent,
                0
        );

        Intent conversationIntent = new Intent(context, ConversationActivity.class);
        conversationIntent.putExtra(Conversation.SERVER_ID, conversation.mServerId);
        conversationIntent.putExtra("accept_invitation", invitation.mId);
        conversationIntent.putExtra("notification_id", mNotificationId);
        PendingIntent pendingAcceptIntent = PendingIntent.getActivity(
                context,
                conversation.mServerId,
                conversationIntent,
                0
        );

        Intent declineInvitationIntent = new Intent(context, MainActivity.class);
        declineInvitationIntent.putExtra("decline_invitation", invitation.mId);
        declineInvitationIntent.putExtra("notification_id", mNotificationId);
        PendingIntent pendingDeclineInvitationIntent = PendingIntent.getActivity(
                context,
                conversation.mServerId,
                declineInvitationIntent,
                0
        );

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setAutoCancel(true);
        mBuilder.addAction(R.drawable.ic_done_white_24px, "Accept", pendingAcceptIntent)
                .addAction(R.drawable.ic_clear_white_24px, "Decline", pendingDeclineInvitationIntent);
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }
}
