package com.mecolab.memeticameandroid.GCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.mecolab.memeticameandroid.Activities.ConversationActivity;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Mecolab on 01-11-2016.
 */

public class ConversationBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String conversationTitle = intent.getStringExtra("conversationTitle");
        String content = intent.getStringExtra("content");
        String sender = intent.getStringExtra("sender");
        int conversationId = intent.getIntExtra("CONVERSATION_ID", -1);
        sendNotification(context, conversationTitle, conversationId, content, sender);
    }


    private void sendNotification(Context context, String title, int conversationId, String content, String sender) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(User.getUserOrCreate(context, sender).mName +": " + content);
        // Sets an ID for the notification
        int mNotificationId = conversationId;
        Intent conversationIntent = new Intent(context, ConversationActivity.class);
        conversationIntent.putExtra(Conversation.SERVER_ID, conversationId);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                conversationId,
                conversationIntent,
                0
        );
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setAutoCancel(true);
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }
}
