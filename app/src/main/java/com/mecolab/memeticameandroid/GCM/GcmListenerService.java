package com.mecolab.memeticameandroid.GCM;

import android.app.NotificationManager;

import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.mecolab.memeticameandroid.Utils.FileManager;
import com.mecolab.memeticameandroid.MemeticameApplication;
import com.mecolab.memeticameandroid.Models.Conversation;
import com.mecolab.memeticameandroid.Models.Invitation;
import com.mecolab.memeticameandroid.Models.Message;
import com.mecolab.memeticameandroid.Models.User;
import com.mecolab.memeticameandroid.Networking.NetworkingManager;
import com.mecolab.memeticameandroid.R;

import java.util.ArrayList;


/**
 * Created by Andres on 04-11-2015.
 */
public class GcmListenerService extends com.google.android.gms.gcm.GcmListenerService {
    private static final String TAG = "GcmListenerService";

    @Override
    public void onMessageReceived(String from, final Bundle data) {

        Log.d(TAG, "From: " + from);

        if (data.getString("collapse_key").equals("new_message")) {
            int conversationId = Integer.valueOf(data.getString("conversation_id"));
            String content = data.getString("message");
            String sender = data.getString("sender");
            int id = Integer.valueOf(data.getString("id"));
            Conversation conversation = Conversation.getConversation(this, conversationId);
            String conversationTitle = "New conversation";
            if (conversation != null) {
                Message.Builder builder = new Message.Builder();
                conversationTitle = conversation.mTitle;
                builder.setSender(sender)
                        .setConversationId(conversationId)
                        .setServerId(id);
                if (data.getString("mime_type") != null) {
                    if (data.getString("mime_type").equals("plain/text")) {
                        builder.setContent(content).setMimeType("plain/text");
                    } else {
                        //revisar peso, esperar a confirmación si se quiere bajar
                        int size = data.getInt("size");

                        Log.d("FileSize", size + "");
                        if (size / 1000 > 100) {
                            Log.d("HeavyFile", "File of size " + size + " is heavy");

                            builder.setMimeType(data.getString("mime_type"))
                                    .setContent(NetworkingManager.downloadFile(data.getString("link"), FileManager.BASE_PATH +
                                            "/" + FileManager.generateFileName(data.getString("mime_type"))).toString());
                            content = "File received";
                        }
                        else{
                            builder.setMimeType(data.getString("mime_type"))
                                    .setContent(NetworkingManager.downloadFile(data.getString("link"), FileManager.BASE_PATH +
                                            "/" + FileManager.generateFileName(data.getString("mime_type"))).toString());
                            content = "File received";
                        }
                    }
                    final Message message = builder.build();
                    if (message.save(this)) {
                        Intent intent = new Intent();
                        intent.putExtra("SERVER_ID", message.mServerId);
                        intent.putExtra("CONVERSATION_ID", conversationId);
                        intent.putExtra("conversationTitle", conversationTitle);
                        intent.putExtra("content", content);
                        intent.putExtra("sender", sender);
                        intent.setAction("com.mecolab.memeticameandroid.MESSAGE_RECEIVED_ACTION");
                        sendOrderedBroadcast(intent, null);
                    }
                }
            }
        } else if (data.getString("collapse_key").equals("new_group_conversation")) {
            int conversationId = Integer.valueOf(data.getString("conversation_id"));
            String title = data.getString("title");
            String admin = data.getString("admin");
            String createdAt = data.getString("created_at");
            String participantsString = data.getString("participants");
            String[] participantsNumber = participantsString.split(",");
            ArrayList<User> participants = new ArrayList<>();
            for (String number : participantsNumber) {
                participants.add(User.getUserOrCreate(this, number.replaceAll("\\D+", "")));
            }
            if (Conversation.getConversation(this, conversationId) == null) {
                Conversation conversation = new Conversation(0, conversationId, title, admin, createdAt,
                        true, participants);
                conversation.save(this);
            }
        } else if (data.getString("collapse_key").equals("new_two_conversation")) {
            int conversationId = Integer.valueOf(data.getString("conversation_id"));
            String createdAt = data.getString("created_at");
            String participantsString = data.getString("participants");
            String[] participantsNumber = participantsString.split(",");
            ArrayList<User> participants = new ArrayList<>();
            for (String number : participantsNumber) {
                participants.add(User.getUserOrCreate(this, number.replaceAll("\\D+", "")));
            }
            User admin = participants.get(1);
            User other = participants.get(0);
            if (participants.get(0).mPhoneNumber.equals(User.getLoggedUser(this).mPhoneNumber)) {
                admin = participants.get(0);
                other = participants.get(1);
            }

            if (Conversation.getConversation(this, conversationId) == null) {
                Conversation conversation = new Conversation(0, conversationId, other.mPhoneNumber,
                        admin.mPhoneNumber, createdAt, true, participants);
                conversation.save(this);
            }
        } else if (data.getString("collapse_key").equals("add_user") && data.getString("group").equals("true")) {
            int conversationId = Integer.valueOf(data.getString("conversation_id"));
            String title = data.getString("title");
            String admin = data.getString("admin");
            String createdAt = data.getString("created_at");
            String participantsString = data.getString("participants");
            String[] participantsNumber = participantsString.split(",");
            ArrayList<User> participants = new ArrayList<>();
            for (String number : participantsNumber) {
                participants.add(User.getUserOrCreate(this, number.replaceAll("\\D+", "")));
            }
            Conversation conversation = new Conversation(0, conversationId, title, admin, createdAt,
                    true, participants);
            conversation.save(this);
            Invitation invitation = new Invitation(0, conversation, Invitation.INVITATION_PENDING, User.getLoggedUser(this));
            long result = invitation.save(this);
            if (result != -1) {
                Intent intent = new Intent();
                intent.putExtra("conversation_id", conversation.mServerId);
                intent.putExtra("user_phone", invitation.mUser.mPhoneNumber);
                intent.putExtra("invitation_id", result);
                intent.setAction("com.mecolab.memeticameandroid.INVITATION_RECEIVED_ACTION");
                sendOrderedBroadcast(intent, null);
            }
        }
    }

    private void sendNotification(String title, String content, String sender) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(User.getUserOrCreate(this, sender).mName + ": " + content);
        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }


}
