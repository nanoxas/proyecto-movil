package com.etiennelawlor.imagegallery.library.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.etiennelawlor.imagegallery.library.R;
import com.etiennelawlor.imagegallery.library.util.RotationGestureDetector;
import com.etiennelawlor.imagegallery.library.util.VarsB;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;

import ly.img.android.ImgLySdk;
import ly.img.android.sdk.models.state.EditorLoadSettings;
import ly.img.android.sdk.models.state.EditorSaveSettings;
import ly.img.android.sdk.models.state.manager.SettingsList;
import ly.img.android.ui.activities.CameraPreviewActivity;
import ly.img.android.ui.activities.PhotoEditorBuilder;
import ly.img.android.ui.utilities.PermissionRequest;


public class CreateMemeActivity extends AppCompatActivity implements
        PermissionRequest.Response,
        RotationGestureDetector.OnRotationGestureListener {

    private ImageView mMeme;
    private EditText mMemeName;

    private RotationGestureDetector mRotationDetector;
    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_meme);

        mMeme = (ImageView) findViewById(R.id.meme);
        //mMeme.setScaleType(ImageView.ScaleType.FIT_XY);
        mMemeName = (EditText) findViewById(R.id.meme_name);

        if (savedInstanceState != null) {
            mMemeName.setText(savedInstanceState.getString("imageName"));
            Bitmap bitmap = savedInstanceState.getParcelable("image");
            mMeme.setImageBitmap(bitmap);
        } else {
            mMemeName.setText(getIntent().getStringExtra("fName"));
            Bitmap bit = BitmapFactory.decodeFile(VarsB.selectedMemeUrl);
            mMeme.setImageBitmap(bit);
        }
        mMemeName.setSelection(mMemeName.getText().length());

        mRotationDetector = new RotationGestureDetector(this);
        mScaleDetector = new ScaleGestureDetector(this, new ScaleListener());


        //Borrar imagen de la uri.
        File file = new File(VarsB.selectedMemeUrl);
        file.delete();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("imageName", mMemeName.getText().toString());
        BitmapDrawable drawable = (BitmapDrawable) mMeme.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        outState.putParcelable("image", bitmap);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_creatememe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.save_meme) {

            String filename = "/storage/emulated/0/MemeticaMe/" + mMemeName.getText();

            //Result bitmap
            Bitmap bmp = ((BitmapDrawable) mMeme.getDrawable()).getBitmap();

            FileOutputStream out = null;
            try {
                out = new FileOutputStream(filename + ".png");
                bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                        Context context = getApplicationContext();
                        CharSequence text = "Meme created sucessfully";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        finish();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            //Remove 2 previous stack activites.
            //Start gallerie updated
            Intent intent = new Intent(getBaseContext(), ImageGalleryActivity.class);
            File dir = new File("/storage/emulated/0/MemeticaMe");
            File[] listOfFiles = dir.listFiles();
            ArrayList<String> images = new ArrayList<>();
            for (int i = 0; i < listOfFiles.length; i++) {
                String mimeType = URLConnection.guessContentTypeFromName(listOfFiles[i].getName());
                if (mimeType != null) {
                    if (listOfFiles[i].isFile() && mimeType.contains("image")) {
                        images.add(listOfFiles[i].getAbsolutePath());
                    }
                }
            }
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, images);
            bundle.putString(ImageGalleryActivity.KEY_TITLE, "Gallery");
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            return true;

        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    //Important for Android 6.0 and above permission request, don't forget this!
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionRequest.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void permissionGranted() {

    }

    @Override
    public void permissionDenied() {
        // The Permission was rejected by the user, so the Editor was not opened because it can not save the result image.
        // TODO for you: Show a Hint to the User
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //mRotationDetector.onTouchEvent(event);
        //mScaleDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public void OnRotation(RotationGestureDetector detector) {
        float angle = detector.getAngle();
        mMeme.setRotation(mMeme.getRotation() + (-angle));
    }


    private class ScaleListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));

            //matrix.setScale(mScaleFactor, mScaleFactor);
            //mMeme.setImageMatrix(matrix);
            mMeme.invalidate();

            /*
            int bitmapWidth = mMeme.getDrawable().getIntrinsicWidth(); //this is the bitmap's width
            int bitmapHeight = mMeme.getDrawable().getIntrinsicHeight(); //this is the bitmap's height*/
            //Log.d("BITMAP CHANGED", "This is the new size: " + mMeme.getMatri + "x" + mMeme.getHeight());
            return true;
        }
    }
}
