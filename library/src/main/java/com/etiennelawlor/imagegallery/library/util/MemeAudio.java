package com.etiennelawlor.imagegallery.library.util;

/**
 * Created by Gabriel on 18/10/2016.
 */

public class MemeAudio {

    public String audioPath;
    public String photoPath;
    Compressor c;
    public String name;

    public MemeAudio(String audioPath, String photoPath, String name) {
        this.audioPath = audioPath;
        this.photoPath = photoPath;
        this.name = name;

    }

    public void Compress(){
        String[] files = {audioPath, photoPath};
        c = new Compressor(files, name);
        c.zip();
    }

}
